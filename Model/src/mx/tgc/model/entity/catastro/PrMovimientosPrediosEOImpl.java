package mx.tgc.model.entity.catastro;

import mx.tgc.model.entity.movimiento.PrTiposMovimientosPrediosEOImpl;
import mx.tgc.utilityfwk.model.extension.entity.GenericEntityImpl;

import oracle.jbo.Key;
import oracle.jbo.RowIterator;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jul 12 09:44:11 MDT 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PrMovimientosPrediosEOImpl extends GenericEntityImpl {
    private static PrMovimientosPrediosEODefImpl mDefinitionObject;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. Do not modify.
     */
    public enum AttributesEnum {
        Serie {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getSerie();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setSerie((Number)value);
            }
        }
        ,
        Identificador {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getIdentificador();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setIdentificador((Number)value);
            }
        }
        ,
        FechaMovimiento {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getFechaMovimiento();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setFechaMovimiento((Date)value);
            }
        }
        ,
        Observaciones {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getObservaciones();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setObservaciones((String)value);
            }
        }
        ,
        TextoCertificado {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getTextoCertificado();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setTextoCertificado((String)value);
            }
        }
        ,
        Estatus {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getEstatus();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setEstatus((String)value);
            }
        }
        ,
        PrPredSerie {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrPredSerie();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setPrPredSerie((Number)value);
            }
        }
        ,
        PrPredIdentificador {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrPredIdentificador();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setPrPredIdentificador((Number)value);
            }
        }
        ,
        PrTmprSerie {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrTmprSerie();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setPrTmprSerie((Number)value);
            }
        }
        ,
        PrTmprIdentificador {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrTmprIdentificador();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setPrTmprIdentificador((Number)value);
            }
        }
        ,
        AaUsuaUsuario {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getAaUsuaUsuario();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setAaUsuaUsuario((String)value);
            }
        }
        ,
        PrPredSerieSegundo {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrPredSerieSegundo();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setPrPredSerieSegundo((Number)value);
            }
        }
        ,
        PrPredIdentificadorSegundo {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrPredIdentificadorSegundo();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setPrPredIdentificadorSegundo((Number)value);
            }
        }
        ,
        Campo1 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo1();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo1((String)value);
            }
        }
        ,
        Campo2 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo2();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo2((String)value);
            }
        }
        ,
        Campo3 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo3();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo3((String)value);
            }
        }
        ,
        Campo4 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo4();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo4((String)value);
            }
        }
        ,
        Campo5 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo5();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo5((String)value);
            }
        }
        ,
        Campo6 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo6();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo6((String)value);
            }
        }
        ,
        Campo7 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo7();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo7((String)value);
            }
        }
        ,
        Campo8 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo8();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo8((String)value);
            }
        }
        ,
        Campo9 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo9();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo9((String)value);
            }
        }
        ,
        Campo10 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo10();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo10((String)value);
            }
        }
        ,
        Campo11 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo11();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo11((String)value);
            }
        }
        ,
        Campo12 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo12();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo12((String)value);
            }
        }
        ,
        Campo13 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo13();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo13((String)value);
            }
        }
        ,
        Campo14 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo14();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo14((String)value);
            }
        }
        ,
        Campo15 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCampo15();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCampo15((String)value);
            }
        }
        ,
        CreadoPor {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCreadoPor();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCreadoPor((String)value);
            }
        }
        ,
        CreadoEl {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getCreadoEl();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setCreadoEl((Date)value);
            }
        }
        ,
        ModificadoPor {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getModificadoPor();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setModificadoPor((String)value);
            }
        }
        ,
        ModificadoEl {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getModificadoEl();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setModificadoEl((Date)value);
            }
        }
        ,
        PrCaracteristicasPredios {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrCaracteristicasPredios();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PrPredios {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrPredios();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setPrPredios((PrPrediosEOImpl)value);
            }
        }
        ,
        PrPredios1 {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrPredios1();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setPrPredios1((PrPrediosEOImpl)value);
            }
        }
        ,
        PrAuditoriasPrediales {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrAuditoriasPrediales();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PrAvaluos {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrAvaluos();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PrCaractPrediosComponentes {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrCaractPrediosComponentes();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PrTiposMovimientosPredios {
            public Object get(PrMovimientosPrediosEOImpl obj) {
                return obj.getPrTiposMovimientosPredios();
            }

            public void put(PrMovimientosPrediosEOImpl obj, Object value) {
                obj.setPrTiposMovimientosPredios((PrTiposMovimientosPrediosEOImpl)value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static int firstIndex = 0;

        public abstract Object get(PrMovimientosPrediosEOImpl object);

        public abstract void put(PrMovimientosPrediosEOImpl object,
                                 Object value);

        public int index() {
            return PrMovimientosPrediosEOImpl.AttributesEnum.firstIndex() + ordinal();
        }

        public static int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return PrMovimientosPrediosEOImpl.AttributesEnum.firstIndex() + PrMovimientosPrediosEOImpl.AttributesEnum.staticValues().length;
        }

        public static AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = PrMovimientosPrediosEOImpl.AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int SERIE = AttributesEnum.Serie.index();
    public static final int IDENTIFICADOR = AttributesEnum.Identificador.index();
    public static final int FECHAMOVIMIENTO = AttributesEnum.FechaMovimiento.index();
    public static final int OBSERVACIONES = AttributesEnum.Observaciones.index();
    public static final int TEXTOCERTIFICADO = AttributesEnum.TextoCertificado.index();
    public static final int ESTATUS = AttributesEnum.Estatus.index();
    public static final int PRPREDSERIE = AttributesEnum.PrPredSerie.index();
    public static final int PRPREDIDENTIFICADOR = AttributesEnum.PrPredIdentificador.index();
    public static final int PRTMPRSERIE = AttributesEnum.PrTmprSerie.index();
    public static final int PRTMPRIDENTIFICADOR = AttributesEnum.PrTmprIdentificador.index();
    public static final int AAUSUAUSUARIO = AttributesEnum.AaUsuaUsuario.index();
    public static final int PRPREDSERIESEGUNDO = AttributesEnum.PrPredSerieSegundo.index();
    public static final int PRPREDIDENTIFICADORSEGUNDO = AttributesEnum.PrPredIdentificadorSegundo.index();
    public static final int CAMPO1 = AttributesEnum.Campo1.index();
    public static final int CAMPO2 = AttributesEnum.Campo2.index();
    public static final int CAMPO3 = AttributesEnum.Campo3.index();
    public static final int CAMPO4 = AttributesEnum.Campo4.index();
    public static final int CAMPO5 = AttributesEnum.Campo5.index();
    public static final int CAMPO6 = AttributesEnum.Campo6.index();
    public static final int CAMPO7 = AttributesEnum.Campo7.index();
    public static final int CAMPO8 = AttributesEnum.Campo8.index();
    public static final int CAMPO9 = AttributesEnum.Campo9.index();
    public static final int CAMPO10 = AttributesEnum.Campo10.index();
    public static final int CAMPO11 = AttributesEnum.Campo11.index();
    public static final int CAMPO12 = AttributesEnum.Campo12.index();
    public static final int CAMPO13 = AttributesEnum.Campo13.index();
    public static final int CAMPO14 = AttributesEnum.Campo14.index();
    public static final int CAMPO15 = AttributesEnum.Campo15.index();
    public static final int CREADOPOR = AttributesEnum.CreadoPor.index();
    public static final int CREADOEL = AttributesEnum.CreadoEl.index();
    public static final int MODIFICADOPOR = AttributesEnum.ModificadoPor.index();
    public static final int MODIFICADOEL = AttributesEnum.ModificadoEl.index();
    public static final int PRCARACTERISTICASPREDIOS = AttributesEnum.PrCaracteristicasPredios.index();
    public static final int PRPREDIOS = AttributesEnum.PrPredios.index();
    public static final int PRPREDIOS1 = AttributesEnum.PrPredios1.index();
    public static final int PRAUDITORIASPREDIALES = AttributesEnum.PrAuditoriasPrediales.index();
    public static final int PRAVALUOS = AttributesEnum.PrAvaluos.index();
    public static final int PRCARACTPREDIOSCOMPONENTES = AttributesEnum.PrCaractPrediosComponentes.index();
    public static final int PRTIPOSMOVIMIENTOSPREDIOS = AttributesEnum.PrTiposMovimientosPredios.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PrMovimientosPrediosEOImpl() {
    }


    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        if (mDefinitionObject == null) {
            mDefinitionObject = (PrMovimientosPrediosEODefImpl)EntityDefImpl.findDefObject("mx.tgc.model.entity.catastro.PrMovimientosPrediosEO");
        }
        return mDefinitionObject;
    }

    /**
     * Gets the attribute value for Serie, using the alias name Serie.
     * @return the Serie
     */
    public Number getSerie() {
        return (Number)getAttributeInternal(SERIE);
    }

    /**
     * Sets <code>value</code> as the attribute value for Serie.
     * @param value value to set the Serie
     */
    public void setSerie(Number value) {
        setAttributeInternal(SERIE, value);
    }

    /**
     * Gets the attribute value for Identificador, using the alias name Identificador.
     * @return the Identificador
     */
    public Number getIdentificador() {
        return (Number)getAttributeInternal(IDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for Identificador.
     * @param value value to set the Identificador
     */
    public void setIdentificador(Number value) {
        setAttributeInternal(IDENTIFICADOR, value);
    }

    /**
     * Gets the attribute value for FechaMovimiento, using the alias name FechaMovimiento.
     * @return the FechaMovimiento
     */
    public Date getFechaMovimiento() {
        return (Date)getAttributeInternal(FECHAMOVIMIENTO);
    }

    /**
     * Sets <code>value</code> as the attribute value for FechaMovimiento.
     * @param value value to set the FechaMovimiento
     */
    public void setFechaMovimiento(Date value) {
        setAttributeInternal(FECHAMOVIMIENTO, value);
    }

    /**
     * Gets the attribute value for Observaciones, using the alias name Observaciones.
     * @return the Observaciones
     */
    public String getObservaciones() {
        return (String)getAttributeInternal(OBSERVACIONES);
    }

    /**
     * Sets <code>value</code> as the attribute value for Observaciones.
     * @param value value to set the Observaciones
     */
    public void setObservaciones(String value) {
        setAttributeInternal(OBSERVACIONES, value);
    }

    /**
     * Gets the attribute value for TextoCertificado, using the alias name TextoCertificado.
     * @return the TextoCertificado
     */
    public String getTextoCertificado() {
        return (String)getAttributeInternal(TEXTOCERTIFICADO);
    }

    /**
     * Sets <code>value</code> as the attribute value for TextoCertificado.
     * @param value value to set the TextoCertificado
     */
    public void setTextoCertificado(String value) {
        setAttributeInternal(TEXTOCERTIFICADO, value);
    }

    /**
     * Gets the attribute value for Estatus, using the alias name Estatus.
     * @return the Estatus
     */
    public String getEstatus() {
        return (String)getAttributeInternal(ESTATUS);
    }

    /**
     * Sets <code>value</code> as the attribute value for Estatus.
     * @param value value to set the Estatus
     */
    public void setEstatus(String value) {
        setAttributeInternal(ESTATUS, value);
    }

    /**
     * Gets the attribute value for PrPredSerie, using the alias name PrPredSerie.
     * @return the PrPredSerie
     */
    public Number getPrPredSerie() {
        return (Number)getAttributeInternal(PRPREDSERIE);
    }

    /**
     * Sets <code>value</code> as the attribute value for PrPredSerie.
     * @param value value to set the PrPredSerie
     */
    public void setPrPredSerie(Number value) {
        setAttributeInternal(PRPREDSERIE, value);
    }

    /**
     * Gets the attribute value for PrPredIdentificador, using the alias name PrPredIdentificador.
     * @return the PrPredIdentificador
     */
    public Number getPrPredIdentificador() {
        return (Number)getAttributeInternal(PRPREDIDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for PrPredIdentificador.
     * @param value value to set the PrPredIdentificador
     */
    public void setPrPredIdentificador(Number value) {
        setAttributeInternal(PRPREDIDENTIFICADOR, value);
    }

    /**
     * Gets the attribute value for PrTmprSerie, using the alias name PrTmprSerie.
     * @return the PrTmprSerie
     */
    public Number getPrTmprSerie() {
        return (Number)getAttributeInternal(PRTMPRSERIE);
    }

    /**
     * Sets <code>value</code> as the attribute value for PrTmprSerie.
     * @param value value to set the PrTmprSerie
     */
    public void setPrTmprSerie(Number value) {
        setAttributeInternal(PRTMPRSERIE, value);
    }

    /**
     * Gets the attribute value for PrTmprIdentificador, using the alias name PrTmprIdentificador.
     * @return the PrTmprIdentificador
     */
    public Number getPrTmprIdentificador() {
        return (Number)getAttributeInternal(PRTMPRIDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for PrTmprIdentificador.
     * @param value value to set the PrTmprIdentificador
     */
    public void setPrTmprIdentificador(Number value) {
        setAttributeInternal(PRTMPRIDENTIFICADOR, value);
    }

    /**
     * Gets the attribute value for AaUsuaUsuario, using the alias name AaUsuaUsuario.
     * @return the AaUsuaUsuario
     */
    public String getAaUsuaUsuario() {
        return (String)getAttributeInternal(AAUSUAUSUARIO);
    }

    /**
     * Sets <code>value</code> as the attribute value for AaUsuaUsuario.
     * @param value value to set the AaUsuaUsuario
     */
    public void setAaUsuaUsuario(String value) {
        setAttributeInternal(AAUSUAUSUARIO, value);
    }

    /**
     * Gets the attribute value for PrPredSerieSegundo, using the alias name PrPredSerieSegundo.
     * @return the PrPredSerieSegundo
     */
    public Number getPrPredSerieSegundo() {
        return (Number)getAttributeInternal(PRPREDSERIESEGUNDO);
    }

    /**
     * Sets <code>value</code> as the attribute value for PrPredSerieSegundo.
     * @param value value to set the PrPredSerieSegundo
     */
    public void setPrPredSerieSegundo(Number value) {
        setAttributeInternal(PRPREDSERIESEGUNDO, value);
    }

    /**
     * Gets the attribute value for PrPredIdentificadorSegundo, using the alias name PrPredIdentificadorSegundo.
     * @return the PrPredIdentificadorSegundo
     */
    public Number getPrPredIdentificadorSegundo() {
        return (Number)getAttributeInternal(PRPREDIDENTIFICADORSEGUNDO);
    }

    /**
     * Sets <code>value</code> as the attribute value for PrPredIdentificadorSegundo.
     * @param value value to set the PrPredIdentificadorSegundo
     */
    public void setPrPredIdentificadorSegundo(Number value) {
        setAttributeInternal(PRPREDIDENTIFICADORSEGUNDO, value);
    }

    /**
     * Gets the attribute value for Campo1, using the alias name Campo1.
     * @return the Campo1
     */
    public String getCampo1() {
        return (String)getAttributeInternal(CAMPO1);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo1.
     * @param value value to set the Campo1
     */
    public void setCampo1(String value) {
        setAttributeInternal(CAMPO1, value);
    }

    /**
     * Gets the attribute value for Campo2, using the alias name Campo2.
     * @return the Campo2
     */
    public String getCampo2() {
        return (String)getAttributeInternal(CAMPO2);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo2.
     * @param value value to set the Campo2
     */
    public void setCampo2(String value) {
        setAttributeInternal(CAMPO2, value);
    }

    /**
     * Gets the attribute value for Campo3, using the alias name Campo3.
     * @return the Campo3
     */
    public String getCampo3() {
        return (String)getAttributeInternal(CAMPO3);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo3.
     * @param value value to set the Campo3
     */
    public void setCampo3(String value) {
        setAttributeInternal(CAMPO3, value);
    }

    /**
     * Gets the attribute value for Campo4, using the alias name Campo4.
     * @return the Campo4
     */
    public String getCampo4() {
        return (String)getAttributeInternal(CAMPO4);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo4.
     * @param value value to set the Campo4
     */
    public void setCampo4(String value) {
        setAttributeInternal(CAMPO4, value);
    }

    /**
     * Gets the attribute value for Campo5, using the alias name Campo5.
     * @return the Campo5
     */
    public String getCampo5() {
        return (String)getAttributeInternal(CAMPO5);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo5.
     * @param value value to set the Campo5
     */
    public void setCampo5(String value) {
        setAttributeInternal(CAMPO5, value);
    }

    /**
     * Gets the attribute value for Campo6, using the alias name Campo6.
     * @return the Campo6
     */
    public String getCampo6() {
        return (String)getAttributeInternal(CAMPO6);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo6.
     * @param value value to set the Campo6
     */
    public void setCampo6(String value) {
        setAttributeInternal(CAMPO6, value);
    }

    /**
     * Gets the attribute value for Campo7, using the alias name Campo7.
     * @return the Campo7
     */
    public String getCampo7() {
        return (String)getAttributeInternal(CAMPO7);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo7.
     * @param value value to set the Campo7
     */
    public void setCampo7(String value) {
        setAttributeInternal(CAMPO7, value);
    }

    /**
     * Gets the attribute value for Campo8, using the alias name Campo8.
     * @return the Campo8
     */
    public String getCampo8() {
        return (String)getAttributeInternal(CAMPO8);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo8.
     * @param value value to set the Campo8
     */
    public void setCampo8(String value) {
        setAttributeInternal(CAMPO8, value);
    }

    /**
     * Gets the attribute value for Campo9, using the alias name Campo9.
     * @return the Campo9
     */
    public String getCampo9() {
        return (String)getAttributeInternal(CAMPO9);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo9.
     * @param value value to set the Campo9
     */
    public void setCampo9(String value) {
        setAttributeInternal(CAMPO9, value);
    }

    /**
     * Gets the attribute value for Campo10, using the alias name Campo10.
     * @return the Campo10
     */
    public String getCampo10() {
        return (String)getAttributeInternal(CAMPO10);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo10.
     * @param value value to set the Campo10
     */
    public void setCampo10(String value) {
        setAttributeInternal(CAMPO10, value);
    }

    /**
     * Gets the attribute value for Campo11, using the alias name Campo11.
     * @return the Campo11
     */
    public String getCampo11() {
        return (String)getAttributeInternal(CAMPO11);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo11.
     * @param value value to set the Campo11
     */
    public void setCampo11(String value) {
        setAttributeInternal(CAMPO11, value);
    }

    /**
     * Gets the attribute value for Campo12, using the alias name Campo12.
     * @return the Campo12
     */
    public String getCampo12() {
        return (String)getAttributeInternal(CAMPO12);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo12.
     * @param value value to set the Campo12
     */
    public void setCampo12(String value) {
        setAttributeInternal(CAMPO12, value);
    }

    /**
     * Gets the attribute value for Campo13, using the alias name Campo13.
     * @return the Campo13
     */
    public String getCampo13() {
        return (String)getAttributeInternal(CAMPO13);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo13.
     * @param value value to set the Campo13
     */
    public void setCampo13(String value) {
        setAttributeInternal(CAMPO13, value);
    }

    /**
     * Gets the attribute value for Campo14, using the alias name Campo14.
     * @return the Campo14
     */
    public String getCampo14() {
        return (String)getAttributeInternal(CAMPO14);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo14.
     * @param value value to set the Campo14
     */
    public void setCampo14(String value) {
        setAttributeInternal(CAMPO14, value);
    }

    /**
     * Gets the attribute value for Campo15, using the alias name Campo15.
     * @return the Campo15
     */
    public String getCampo15() {
        return (String)getAttributeInternal(CAMPO15);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo15.
     * @param value value to set the Campo15
     */
    public void setCampo15(String value) {
        setAttributeInternal(CAMPO15, value);
    }

    /**
     * Gets the attribute value for CreadoPor, using the alias name CreadoPor.
     * @return the CreadoPor
     */
    public String getCreadoPor() {
        return (String)getAttributeInternal(CREADOPOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for CreadoPor.
     * @param value value to set the CreadoPor
     */
    public void setCreadoPor(String value) {
        setAttributeInternal(CREADOPOR, value);
    }

    /**
     * Gets the attribute value for CreadoEl, using the alias name CreadoEl.
     * @return the CreadoEl
     */
    public Date getCreadoEl() {
        return (Date)getAttributeInternal(CREADOEL);
    }

    /**
     * Sets <code>value</code> as the attribute value for CreadoEl.
     * @param value value to set the CreadoEl
     */
    public void setCreadoEl(Date value) {
        setAttributeInternal(CREADOEL, value);
    }

    /**
     * Gets the attribute value for ModificadoPor, using the alias name ModificadoPor.
     * @return the ModificadoPor
     */
    public String getModificadoPor() {
        return (String)getAttributeInternal(MODIFICADOPOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for ModificadoPor.
     * @param value value to set the ModificadoPor
     */
    public void setModificadoPor(String value) {
        setAttributeInternal(MODIFICADOPOR, value);
    }

    /**
     * Gets the attribute value for ModificadoEl, using the alias name ModificadoEl.
     * @return the ModificadoEl
     */
    public Date getModificadoEl() {
        return (Date)getAttributeInternal(MODIFICADOEL);
    }

    /**
     * Sets <code>value</code> as the attribute value for ModificadoEl.
     * @param value value to set the ModificadoEl
     */
    public void setModificadoEl(Date value) {
        setAttributeInternal(MODIFICADOEL, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index,
                                           AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value,
                                         AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }

    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getPrCaracteristicasPredios() {
        return (RowIterator)getAttributeInternal(PRCARACTERISTICASPREDIOS);
    }

    /**
     * @return the associated entity PrPrediosEOImpl.
     */
    public PrPrediosEOImpl getPrPredios() {
        return (PrPrediosEOImpl)getAttributeInternal(PRPREDIOS);
    }

    /**
     * Sets <code>value</code> as the associated entity PrPrediosEOImpl.
     */
    public void setPrPredios(PrPrediosEOImpl value) {
        setAttributeInternal(PRPREDIOS, value);
    }

    /**
     * @return the associated entity PrPrediosEOImpl.
     */
    public PrPrediosEOImpl getPrPredios1() {
        return (PrPrediosEOImpl)getAttributeInternal(PRPREDIOS1);
    }

    /**
     * Sets <code>value</code> as the associated entity PrPrediosEOImpl.
     */
    public void setPrPredios1(PrPrediosEOImpl value) {
        setAttributeInternal(PRPREDIOS1, value);
    }

    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getPrAuditoriasPrediales() {
        return (RowIterator)getAttributeInternal(PRAUDITORIASPREDIALES);
    }

    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getPrAvaluos() {
        return (RowIterator)getAttributeInternal(PRAVALUOS);
    }

    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getPrCaractPrediosComponentes() {
        return (RowIterator)getAttributeInternal(PRCARACTPREDIOSCOMPONENTES);
    }

    /**
     * @return the associated entity PrTiposMovimientosPrediosEOImpl.
     */
    public PrTiposMovimientosPrediosEOImpl getPrTiposMovimientosPredios() {
        return (PrTiposMovimientosPrediosEOImpl)getAttributeInternal(PRTIPOSMOVIMIENTOSPREDIOS);
    }

    /**
     * Sets <code>value</code> as the associated entity PrTiposMovimientosPrediosEOImpl.
     */
    public void setPrTiposMovimientosPredios(PrTiposMovimientosPrediosEOImpl value) {
        setAttributeInternal(PRTIPOSMOVIMIENTOSPREDIOS, value);
    }

    /**
     * @param serie key constituent
     * @param identificador key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(Number serie, Number identificador) {
        return new Key(new Object[]{serie, identificador});
    }


}

package mx.tgc.model.ro.ubicacion;

import mx.tgc.utilityfwk.model.extension.view.GenericViewImpl;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Aug 19 12:44:30 MDT 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PcMunicipiosVVOImpl extends GenericViewImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public PcMunicipiosVVOImpl() {
    }

    /**
     * Returns the variable value for pcMuniIdentificador.
     * @return variable value for pcMuniIdentificador
     */
    public String getpcMuniIdentificador() {
        return (String)ensureVariableManager().getVariableValue("pcMuniIdentificador");
    }

    /**
     * Sets <code>value</code> for variable pcMuniIdentificador.
     * @param value value to bind as pcMuniIdentificador
     */
    public void setpcMuniIdentificador(String value) {
        ensureVariableManager().setVariableValue("pcMuniIdentificador", value);
    }
}

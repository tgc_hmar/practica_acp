package mx.tgc.model.ro.ubicacion.client;

import java.util.List;

import mx.tgc.model.ro.ubicacion.common.PcMunicipiosRecaudacionesCustomVVO;

import oracle.jbo.client.remote.ViewUsageImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Sep 07 11:20:06 MDT 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PcMunicipiosRecaudacionesCustomVVOClient extends ViewUsageImpl implements PcMunicipiosRecaudacionesCustomVVO {
    /**
     * This is the default constructor (do not remove).
     */
    public PcMunicipiosRecaudacionesCustomVVOClient() {
    }

    public void buscar(List p0) {
        Object _ret =
            getApplicationModuleProxy().riInvokeExportedMethod(this,"buscar",new String [] {"java.util.List"},new Object[] {p0});
        return;
    }
}

package mx.tgc.model.ro.ubicacion;

import oracle.jbo.RowIterator;
import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Jul 03 15:49:08 MDT 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AaUsuariosMunicipiosVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. Do not modify.
     */
    public enum AttributesEnum {
        Identificador {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getIdentificador();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setIdentificador((Number)value);
            }
        }
        ,
        PcEstaIdentificador {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getPcEstaIdentificador();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setPcEstaIdentificador((Number)value);
            }
        }
        ,
        ClaveMunicipio {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getClaveMunicipio();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setClaveMunicipio((String)value);
            }
        }
        ,
        Descripcion {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getDescripcion();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setDescripcion((String)value);
            }
        }
        ,
        NombreCorto {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getNombreCorto();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setNombreCorto((String)value);
            }
        }
        ,
        CabeceraMunicipal {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getCabeceraMunicipal();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setCabeceraMunicipal((String)value);
            }
        }
        ,
        Estatus {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getEstatus();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setEstatus((String)value);
            }
        }
        ,
        Campo1 {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getCampo1();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setCampo1((String)value);
            }
        }
        ,
        Campo2 {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getCampo2();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setCampo2((String)value);
            }
        }
        ,
        Campo3 {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getCampo3();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setCampo3((String)value);
            }
        }
        ,
        Campo4 {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getCampo4();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setCampo4((String)value);
            }
        }
        ,
        Campo5 {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getCampo5();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setCampo5((String)value);
            }
        }
        ,
        PcLocalidadesV {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getPcLocalidadesV();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PrSegmentosCatastrales {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getPrSegmentosCatastrales();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PrGiros1 {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getPrGiros1();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PrTiposAvaluos {
            public Object get(AaUsuariosMunicipiosVVORowImpl obj) {
                return obj.getPrTiposAvaluos();
            }

            public void put(AaUsuariosMunicipiosVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static int firstIndex = 0;

        public abstract Object get(AaUsuariosMunicipiosVVORowImpl object);

        public abstract void put(AaUsuariosMunicipiosVVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int IDENTIFICADOR = AttributesEnum.Identificador.index();
    public static final int PCESTAIDENTIFICADOR = AttributesEnum.PcEstaIdentificador.index();
    public static final int CLAVEMUNICIPIO = AttributesEnum.ClaveMunicipio.index();
    public static final int DESCRIPCION = AttributesEnum.Descripcion.index();
    public static final int NOMBRECORTO = AttributesEnum.NombreCorto.index();
    public static final int CABECERAMUNICIPAL = AttributesEnum.CabeceraMunicipal.index();
    public static final int ESTATUS = AttributesEnum.Estatus.index();
    public static final int CAMPO1 = AttributesEnum.Campo1.index();
    public static final int CAMPO2 = AttributesEnum.Campo2.index();
    public static final int CAMPO3 = AttributesEnum.Campo3.index();
    public static final int CAMPO4 = AttributesEnum.Campo4.index();
    public static final int CAMPO5 = AttributesEnum.Campo5.index();
    public static final int PCLOCALIDADESV = AttributesEnum.PcLocalidadesV.index();
    public static final int PRSEGMENTOSCATASTRALES = AttributesEnum.PrSegmentosCatastrales.index();
    public static final int PRGIROS1 = AttributesEnum.PrGiros1.index();
    public static final int PRTIPOSAVALUOS = AttributesEnum.PrTiposAvaluos.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AaUsuariosMunicipiosVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Identificador.
     * @return the Identificador
     */
    public Number getIdentificador() {
        return (Number) getAttributeInternal(IDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Identificador.
     * @param value value to set the  Identificador
     */
    public void setIdentificador(Number value) {
        setAttributeInternal(IDENTIFICADOR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PcEstaIdentificador.
     * @return the PcEstaIdentificador
     */
    public Number getPcEstaIdentificador() {
        return (Number) getAttributeInternal(PCESTAIDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PcEstaIdentificador.
     * @param value value to set the  PcEstaIdentificador
     */
    public void setPcEstaIdentificador(Number value) {
        setAttributeInternal(PCESTAIDENTIFICADOR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ClaveMunicipio.
     * @return the ClaveMunicipio
     */
    public String getClaveMunicipio() {
        return (String) getAttributeInternal(CLAVEMUNICIPIO);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ClaveMunicipio.
     * @param value value to set the  ClaveMunicipio
     */
    public void setClaveMunicipio(String value) {
        setAttributeInternal(CLAVEMUNICIPIO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Descripcion.
     * @return the Descripcion
     */
    public String getDescripcion() {
        return (String) getAttributeInternal(DESCRIPCION);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Descripcion.
     * @param value value to set the  Descripcion
     */
    public void setDescripcion(String value) {
        setAttributeInternal(DESCRIPCION, value);
    }

    /**
     * Gets the attribute value for the calculated attribute NombreCorto.
     * @return the NombreCorto
     */
    public String getNombreCorto() {
        return (String) getAttributeInternal(NOMBRECORTO);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute NombreCorto.
     * @param value value to set the  NombreCorto
     */
    public void setNombreCorto(String value) {
        setAttributeInternal(NOMBRECORTO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CabeceraMunicipal.
     * @return the CabeceraMunicipal
     */
    public String getCabeceraMunicipal() {
        return (String) getAttributeInternal(CABECERAMUNICIPAL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CabeceraMunicipal.
     * @param value value to set the  CabeceraMunicipal
     */
    public void setCabeceraMunicipal(String value) {
        setAttributeInternal(CABECERAMUNICIPAL, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Estatus.
     * @return the Estatus
     */
    public String getEstatus() {
        return (String) getAttributeInternal(ESTATUS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Estatus.
     * @param value value to set the  Estatus
     */
    public void setEstatus(String value) {
        setAttributeInternal(ESTATUS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Campo1.
     * @return the Campo1
     */
    public String getCampo1() {
        return (String) getAttributeInternal(CAMPO1);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Campo1.
     * @param value value to set the  Campo1
     */
    public void setCampo1(String value) {
        setAttributeInternal(CAMPO1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Campo2.
     * @return the Campo2
     */
    public String getCampo2() {
        return (String) getAttributeInternal(CAMPO2);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Campo2.
     * @param value value to set the  Campo2
     */
    public void setCampo2(String value) {
        setAttributeInternal(CAMPO2, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Campo3.
     * @return the Campo3
     */
    public String getCampo3() {
        return (String) getAttributeInternal(CAMPO3);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Campo3.
     * @param value value to set the  Campo3
     */
    public void setCampo3(String value) {
        setAttributeInternal(CAMPO3, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Campo4.
     * @return the Campo4
     */
    public String getCampo4() {
        return (String) getAttributeInternal(CAMPO4);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Campo4.
     * @param value value to set the  Campo4
     */
    public void setCampo4(String value) {
        setAttributeInternal(CAMPO4, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Campo5.
     * @return the Campo5
     */
    public String getCampo5() {
        return (String) getAttributeInternal(CAMPO5);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Campo5.
     * @param value value to set the  Campo5
     */
    public void setCampo5(String value) {
        setAttributeInternal(CAMPO5, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link PcLocalidadesV.
     */
    public RowIterator getPcLocalidadesV() {
        return (RowIterator)getAttributeInternal(PCLOCALIDADESV);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link PrSegmentosCatastrales.
     */
    public RowIterator getPrSegmentosCatastrales() {
        return (RowIterator)getAttributeInternal(PRSEGMENTOSCATASTRALES);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link PrGiros1.
     */
    public RowIterator getPrGiros1() {
        return (RowIterator)getAttributeInternal(PRGIROS1);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link PrTiposAvaluos.
     */
    public RowIterator getPrTiposAvaluos() {
        return (RowIterator)getAttributeInternal(PRTIPOSAVALUOS);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

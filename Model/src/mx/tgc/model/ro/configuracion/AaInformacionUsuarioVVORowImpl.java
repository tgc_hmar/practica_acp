package mx.tgc.model.ro.configuracion;

import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Dec 31 15:18:14 MST 2014
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AaInformacionUsuarioVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. Do not modify.
     */
    public enum AttributesEnum {
        Usuario {
            public Object get(AaInformacionUsuarioVVORowImpl obj) {
                return obj.getUsuario();
            }

            public void put(AaInformacionUsuarioVVORowImpl obj, Object value) {
                obj.setUsuario((String)value);
            }
        }
        ,
        UserFullName {
            public Object get(AaInformacionUsuarioVVORowImpl obj) {
                return obj.getUserFullName();
            }

            public void put(AaInformacionUsuarioVVORowImpl obj, Object value) {
                obj.setUserFullName((String)value);
            }
        }
        ,
        RecaudacionId {
            public Object get(AaInformacionUsuarioVVORowImpl obj) {
                return obj.getRecaudacionId();
            }

            public void put(AaInformacionUsuarioVVORowImpl obj, Object value) {
                obj.setRecaudacionId((Number)value);
            }
        }
        ,
        RecaudacionName {
            public Object get(AaInformacionUsuarioVVORowImpl obj) {
                return obj.getRecaudacionName();
            }

            public void put(AaInformacionUsuarioVVORowImpl obj, Object value) {
                obj.setRecaudacionName((String)value);
            }
        }
        ,
        MunicipioId {
            public Object get(AaInformacionUsuarioVVORowImpl obj) {
                return obj.getMunicipioId();
            }

            public void put(AaInformacionUsuarioVVORowImpl obj, Object value) {
                obj.setMunicipioId((Number)value);
            }
        }
        ,
        MunicipioDescripcion {
            public Object get(AaInformacionUsuarioVVORowImpl obj) {
                return obj.getMunicipioDescripcion();
            }

            public void put(AaInformacionUsuarioVVORowImpl obj, Object value) {
                obj.setMunicipioDescripcion((String)value);
            }
        }
        ,
        RecaudacionBase {
            public Object get(AaInformacionUsuarioVVORowImpl obj) {
                return obj.getRecaudacionBase();
            }

            public void put(AaInformacionUsuarioVVORowImpl obj, Object value) {
                obj.setRecaudacionBase((String)value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static int firstIndex = 0;

        public abstract Object get(AaInformacionUsuarioVVORowImpl object);

        public abstract void put(AaInformacionUsuarioVVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int USUARIO = AttributesEnum.Usuario.index();
    public static final int USERFULLNAME = AttributesEnum.UserFullName.index();
    public static final int RECAUDACIONID = AttributesEnum.RecaudacionId.index();
    public static final int RECAUDACIONNAME = AttributesEnum.RecaudacionName.index();
    public static final int MUNICIPIOID = AttributesEnum.MunicipioId.index();
    public static final int MUNICIPIODESCRIPCION = AttributesEnum.MunicipioDescripcion.index();
    public static final int RECAUDACIONBASE = AttributesEnum.RecaudacionBase.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AaInformacionUsuarioVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Usuario.
     * @return the Usuario
     */
    public String getUsuario() {
        return (String) getAttributeInternal(USUARIO);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Usuario.
     * @param value value to set the  Usuario
     */
    public void setUsuario(String value) {
        setAttributeInternal(USUARIO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute UserFullName.
     * @return the UserFullName
     */
    public String getUserFullName() {
        return (String) getAttributeInternal(USERFULLNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UserFullName.
     * @param value value to set the  UserFullName
     */
    public void setUserFullName(String value) {
        setAttributeInternal(USERFULLNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute RecaudacionId.
     * @return the RecaudacionId
     */
    public Number getRecaudacionId() {
        return (Number) getAttributeInternal(RECAUDACIONID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute RecaudacionId.
     * @param value value to set the  RecaudacionId
     */
    public void setRecaudacionId(Number value) {
        setAttributeInternal(RECAUDACIONID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute RecaudacionName.
     * @return the RecaudacionName
     */
    public String getRecaudacionName() {
        return (String) getAttributeInternal(RECAUDACIONNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute RecaudacionName.
     * @param value value to set the  RecaudacionName
     */
    public void setRecaudacionName(String value) {
        setAttributeInternal(RECAUDACIONNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute MunicipioId.
     * @return the MunicipioId
     */
    public Number getMunicipioId() {
        return (Number) getAttributeInternal(MUNICIPIOID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute MunicipioId.
     * @param value value to set the  MunicipioId
     */
    public void setMunicipioId(Number value) {
        setAttributeInternal(MUNICIPIOID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute MunicipioDescripcion.
     * @return the MunicipioDescripcion
     */
    public String getMunicipioDescripcion() {
        return (String) getAttributeInternal(MUNICIPIODESCRIPCION);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute MunicipioDescripcion.
     * @param value value to set the  MunicipioDescripcion
     */
    public void setMunicipioDescripcion(String value) {
        setAttributeInternal(MUNICIPIODESCRIPCION, value);
    }

    /**
     * Gets the attribute value for the calculated attribute RecaudacionBase.
     * @return the RecaudacionBase
     */
    public String getRecaudacionBase() {
        return (String) getAttributeInternal(RECAUDACIONBASE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute RecaudacionBase.
     * @param value value to set the  RecaudacionBase
     */
    public void setRecaudacionBase(String value) {
        setAttributeInternal(RECAUDACIONBASE, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

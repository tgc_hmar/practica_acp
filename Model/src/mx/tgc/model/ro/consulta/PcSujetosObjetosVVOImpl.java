package mx.tgc.model.ro.consulta;

import mx.tgc.utilityfwk.model.extension.view.GenericViewImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Oct 13 12:07:56 MDT 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PcSujetosObjetosVVOImpl extends GenericViewImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public PcSujetosObjetosVVOImpl() {
    }

    /**
     * Returns the variable value for predSerie.
     * @return variable value for predSerie
     */
    public String getpredSerie() {
        return (String)ensureVariableManager().getVariableValue("predSerie");
    }

    /**
     * Sets <code>value</code> for variable predSerie.
     * @param value value to bind as predSerie
     */
    public void setpredSerie(String value) {
        ensureVariableManager().setVariableValue("predSerie", value);
    }

    /**
     * Returns the variable value for predId.
     * @return variable value for predId
     */
    public String getpredId() {
        return (String)ensureVariableManager().getVariableValue("predId");
    }

    /**
     * Sets <code>value</code> for variable predId.
     * @param value value to bind as predId
     */
    public void setpredId(String value) {
        ensureVariableManager().setVariableValue("predId", value);
    }
}

package mx.tgc.model.view.movimiento.client;

import java.util.List;

import mx.tgc.model.view.movimiento.common.PrPredialCaracteristicasVO;

import oracle.jbo.client.remote.ViewUsageImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Oct 03 11:40:26 MDT 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PrPredialCaracteristicasVOClient extends ViewUsageImpl implements PrPredialCaracteristicasVO {
    /**
     * This is the default constructor (do not remove).
     */
    public PrPredialCaracteristicasVOClient() {
    }

    public void buscar(List p0) {
        Object _ret =
            getApplicationModuleProxy().riInvokeExportedMethod(this,"buscar",new String [] {"java.util.List"},new Object[] {p0});
        return;
    }
}

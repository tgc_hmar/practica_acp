package mx.tgc.model.module.view.PrConsultaPredioVVO1VO;

import mx.tgc.model.module.applicationModule.ConsultaAMFixture;

import oracle.jbo.ViewObject;

import org.junit.After;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;

public class PrConsultaPredioVVO1VOTest {
    private ConsultaAMFixture fixture1 = ConsultaAMFixture.getInstance();

    public PrConsultaPredioVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PrConsultaPredioVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
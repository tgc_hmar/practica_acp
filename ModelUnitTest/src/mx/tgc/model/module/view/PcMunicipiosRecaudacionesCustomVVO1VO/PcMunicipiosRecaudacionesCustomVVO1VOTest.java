package mx.tgc.model.module.view.PcMunicipiosRecaudacionesCustomVVO1VO;

import mx.tgc.model.module.applicationModule.CataSegCatastralesAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PcMunicipiosRecaudacionesCustomVVO1VOTest {
    private CataSegCatastralesAMFixture fixture1 = CataSegCatastralesAMFixture.getInstance();

    public PcMunicipiosRecaudacionesCustomVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PcMunicipiosRecaudacionesCustomVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}

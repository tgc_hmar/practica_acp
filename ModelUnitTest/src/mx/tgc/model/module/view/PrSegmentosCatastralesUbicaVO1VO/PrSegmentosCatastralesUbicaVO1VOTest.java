package mx.tgc.model.module.view.PrSegmentosCatastralesUbicaVO1VO;

import mx.tgc.model.module.applicationModule.CataSegCatastralesAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrSegmentosCatastralesUbicaVO1VOTest {
    private CataSegCatastralesAMFixture fixture1 =
        CataSegCatastralesAMFixture.getInstance();

    public PrSegmentosCatastralesUbicaVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PrSegmentosCatastralesUbicaVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
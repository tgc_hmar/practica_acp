package mx.tgc.model.module.view.CcRecaudacionesVVO1VO;

import mx.tgc.model.module.applicationModule.CataAvaluoAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class CcRecaudacionesVVO1VOTest {
    private CataAvaluoAMFixture fixture1 = CataAvaluoAMFixture.getInstance();

    public CcRecaudacionesVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("CcRecaudacionesVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}

package mx.tgc.model.module.view.PrPredBloquesAgrupacionesVO1VO;

import mx.tgc.model.module.CataBloquesPrediosAMImpl;
import mx.tgc.model.module.applicationModule.CataBloquesPrediosAMFixture;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrPredBloquesAgrupacionesVO1VOTest {
    private CataBloquesPrediosAMFixture fixture1 =
        CataBloquesPrediosAMFixture.getInstance();
    private CataBloquesPrediosAMImpl _amImpl =
        (CataBloquesPrediosAMImpl)fixture1.getApplicationModule();

    public PrPredBloquesAgrupacionesVO1VOTest() {
    }

    /**
     * M�todo que sirve para probar la creaci�n de la instancia de la vista.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testCreacionInstancia() {
        ViewObject view = _amImpl.findViewObject("PrPredBloquesAgrupacionesVO2");
        assertNotNull(view);
    }

    /**
     * M�todo que sirve para probar los campos obligatorios
     * verifica que efectivamente si no se llenan los campos obligatorios
     * al validar el nuevo registro debe arrojar error.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testCamposObligatorios() {
        ViewObject view = _amImpl.findViewObject("PrPredBloquesAgrupacionesVO2");
        Row row = view.createRow();

        //TODO: por cada atributo obligatorio en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Identificador", 999999999999999L);
        row.setAttribute("Nombre", "NOMBRE");
        row.setAttribute("Etiqueta", "ETIQUETA");
        row.setAttribute("Orden", 123456789);
        row.setAttribute("Pantalla", 123456789);
        //-------------
        row.validate();
        _amImpl.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar la longitud de los campos
     * de una vista de datos, el m�todo prueba que las longitudes
     * sean probadas con la m�xima longitud permitida para los
     * campos de tipo String (VARCHAR2).
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testPresicionMaximaEnCampos() {
        ViewObject view = _amImpl.findViewObject("PrPredBloquesAgrupacionesVO2");
        Row row = view.createRow();

        //TODO: variables predefinidas con varias longitudes para
        //probar el m�ximo de caracteres permitidos en los campos de las tablas
        String campo2000Chars = "";
        for (int i = 0; i < 2000; i++) {
            campo2000Chars += "1";
        }

        String campo300Chars = "";
        for (int i = 0; i < 300; i++) {
            campo300Chars += "1";
        }

        String campo100Chars = "";
        for (int i = 0; i < 100; i++) {
            campo100Chars += "1";
        }

        String campo30Chars = "";
        for (int i = 0; i < 30; i++) {
            campo30Chars += "1";
        }

        String campo20Chars = "";
        for (int i = 0; i < 20; i++) {
            campo20Chars += "1";
        }

        String campo15Chars = "";
        for (int i = 0; i < 15; i++) {
            campo15Chars += "1";
        }

        String campo10Chars = "";
        for (int i = 0; i < 10; i++) {
            campo10Chars += "1";
        }
        
        String campo9Chars = "";
        for (int i = 0; i < 9; i++) {
            campo9Chars += "1";
        }

        String campo4Chars = "";
        for (int i = 0; i < 4; i++) {
            campo4Chars += "1";
        }

        String campo2Chars = "";
        for (int i = 0; i < 2; i++) {
            campo2Chars += "1";
        }

        //TODO: por cada atributo en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Identificador", Long.parseLong(campo15Chars));
        row.setAttribute("Nombre", campo30Chars);
        row.setAttribute("Etiqueta", campo100Chars);
        row.setAttribute("Orden", campo9Chars);
        row.setAttribute("Pantalla", campo9Chars);
        row.setAttribute("Campo1", campo2000Chars);
        row.setAttribute("Campo2", campo2000Chars);
        row.setAttribute("Campo3", campo2000Chars);
        row.setAttribute("Campo4", campo2000Chars);
        row.setAttribute("Campo5", campo2000Chars);
        row.setAttribute("Campo6", campo2000Chars);
        row.setAttribute("Campo7", campo2000Chars);
        row.setAttribute("Campo8", campo2000Chars);
        row.setAttribute("Campo9", campo2000Chars);
        row.setAttribute("Campo10", campo2000Chars);
        row.setAttribute("Campo11", campo2000Chars);
        row.setAttribute("Campo12", campo2000Chars);
        row.setAttribute("Campo13", campo2000Chars);
        row.setAttribute("Campo14", campo2000Chars);
        row.setAttribute("Campo15", campo2000Chars);
        //-------------------
        row.validate();
        _amImpl.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar altas y
     * bajas de registros en la vista de datos
     * se crea un nuevo row al cual se le llena con datos
     * de prueba para poder probar las operaciones de
     * insert y delete.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testInsertUpdateDelete() {
        ViewObject view = _amImpl.findViewObject("PrPredBloquesAgrupacionesVO2");
        Row row = view.createRow();

        //TODO: Llenar cada atributo de la vista con valores para probar los inserts y deletes
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Identificador", 999999999999999L);
        row.setAttribute("Nombre", "NOMBREBLOQUE");
        row.setAttribute("Etiqueta", "ETIQUETA");
        row.setAttribute("Orden", 123456789);
        row.setAttribute("Pantalla", 123456789);
        _amImpl.getDBTransaction().commit();
        //-------------------
        row.setAttribute("Orden", 123456780);
        _amImpl.getDBTransaction().commit();
        //-------------------
        row.remove();
        _amImpl.getDBTransaction().commit();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}

package mx.tgc.model.module.view.PrPresentacionCaracteristicaVO1VO;

import java.sql.SQLException;
import java.sql.Statement;

import mx.tgc.model.module.CataBloquesPrediosAMImpl;
import mx.tgc.model.module.applicationModule.CataBloquesPrediosAMFixture;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import org.junit.BeforeClass;
import org.junit.Test;


public class PrPresentacionCaracteristicaVO1VOTest {
    private static CataBloquesPrediosAMFixture fixture1 =
        CataBloquesPrediosAMFixture.getInstance();
    private static CataBloquesPrediosAMImpl _amImpl =
        (CataBloquesPrediosAMImpl)fixture1.getApplicationModule();

    public PrPresentacionCaracteristicaVO1VOTest() {
    }

    /**
     * M�todo que sirve para probar la creaci�n de la instancia de la vista.
     *
     * @author Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    @Test
    public void testCreacionInstancia() {
        ViewObject view =
            _amImpl.findViewObject("PrPresentacionCaracteristicaVO1");
        assertNotNull(view);
    }

    /**
     * M�todo que sirve para probar los campos obligatorios
     * verifica que efectivamente si no se llenan los campos obligatorios
     * al validar el nuevo registro debe arrojar error.
     *
     * @author: Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    @Test
    public void testCamposObligatorios() {
        ViewObject view =
            _amImpl.findViewObject("PrPresentacionCaracteristicaVO1");
        Row row = view.createRow();

        //TODO: por cada atributo obligatorio en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("PrPrcaSerie", 9999);
        row.setAttribute("PrPrcaIdentificador", 999999999999999L);
        row.setAttribute("PrPrbaIdentificador", 999999999999999L);
        row.setAttribute("Orden", 1);
        row.setAttribute("Capturable", "S");
        //-------------
        row.validate();
        _amImpl.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar la longitud de los campos
     * de una vista de datos, el m�todo prueba que las longitudes
     * sean probadas con la m�xima longitud permitida para los
     * campos de tipo String (VARCHAR2).
     *
     * @author: Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    @Test
    public void testPresicionMaximaEnCampos() {
        ViewObject view =
            _amImpl.findViewObject("PrPresentacionCaracteristicaVO1");
        Row row = view.createRow();

        //TODO: variables predefinidas con varias longitudes para
        //probar el m�ximo de caracteres permitidos en los campos de las tablas
        String campo2000Chars = "";
        for (int i = 0; i < 2000; i++) {
            campo2000Chars += "1";
        }

        String campo300Chars = "";
        for (int i = 0; i < 300; i++) {
            campo300Chars += "1";
        }

        String campo100Chars = "";
        for (int i = 0; i < 100; i++) {
            campo100Chars += "1";
        }

        String campo30Chars = "";
        for (int i = 0; i < 30; i++) {
            campo30Chars += "1";
        }

        String campo20Chars = "";
        for (int i = 0; i < 20; i++) {
            campo20Chars += "1";
        }

        String campo15Chars = "";
        for (int i = 0; i < 15; i++) {
            campo15Chars += "1";
        }

        String campo10Chars = "";
        for (int i = 0; i < 10; i++) {
            campo10Chars += "1";
        }

        String campo9Chars = "";
        for (int i = 0; i < 9; i++) {
            campo9Chars += "1";
        }

        String campo4Chars = "";
        for (int i = 0; i < 4; i++) {
            campo4Chars += "1";
        }

        String campo2Chars = "";
        for (int i = 0; i < 2; i++) {
            campo2Chars += "1";
        }

        //TODO: por cada atributo en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("PrPrcaSerie", 9999);
        row.setAttribute("PrPrcaIdentificador", 999999999999999L);
        row.setAttribute("PrPrbaIdentificador", 999999999999999L);
        row.setAttribute("Orden", Integer.parseInt(campo9Chars));
        row.setAttribute("Capturable", "S");
        row.setAttribute("Campo1", campo2000Chars);
        row.setAttribute("Campo2", campo2000Chars);
        row.setAttribute("Campo3", campo2000Chars);
        row.setAttribute("Campo4", campo2000Chars);
        row.setAttribute("Campo5", campo2000Chars);
        row.setAttribute("Campo6", campo2000Chars);
        row.setAttribute("Campo7", campo2000Chars);
        row.setAttribute("Campo8", campo2000Chars);
        row.setAttribute("Campo9", campo2000Chars);
        row.setAttribute("Campo10", campo2000Chars);
        row.setAttribute("Campo11", campo2000Chars);
        row.setAttribute("Campo12", campo2000Chars);
        row.setAttribute("Campo13", campo2000Chars);
        row.setAttribute("Campo14", campo2000Chars);
        row.setAttribute("Campo15", campo2000Chars);
        //-------------------
        row.validate();
        _amImpl.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar altas y
     * bajas de registros en la vista de datos
     * se crea un nuevo row al cual se le llena con datos
     * de prueba para poder probar las operaciones de
     * insert y delete.
     *
     * @author: Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    @Test
    public void testInsertUpdateDelete() {
        ViewObject view =
            _amImpl.findViewObject("PrPresentacionCaracteristicaVO1");
        Row row = view.createRow();

        //TODO: Llenar cada atributo de la vista con valores para probar los inserts y deletes
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("PrPrcaSerie", 9999);
        row.setAttribute("PrPrcaIdentificador", 999999999999999L);
        row.setAttribute("PrPrbaIdentificador", 999999999999999L);
        row.setAttribute("Orden", 1);
        row.setAttribute("Capturable", "S");
        _amImpl.getDBTransaction().commit();
        //-------------------
        row.setAttribute("Orden", 2);
        _amImpl.getDBTransaction().commit();
        //-------------------
        row.remove();
        _amImpl.getDBTransaction().commit();
    }

    /**
     * M�todo que mediante sentencias de SQL inserta registros en diversas
     * tablas, necesarios para iniciar las pruebas de la vista.
     *
     * @author Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    @BeforeClass
    public static void setUp() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("Insert into PR_TIPOS_AVALUOS (SERIE,IDENTIFICADOR,TIPO_AVALUO,NOMBRE,DESCRIPCION,ESTATUS,FECHA_INICIO,FECHA_FIN,CC_RECA_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (9999,999999999999999,'TIPOAV','NOMBRE','DESCRIPCION','AC',to_timestamp('20/09/11','DD/MM/RR HH24:MI:SSXFF'),null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'RECAUDADOR',to_timestamp('08/09/11','DD/MM/RR HH24:MI:SSXFF'),'RECAUDADOR',to_timestamp('08/09/11','DD/MM/RR HH24:MI:SSXFF'))");
            stmt.execute("Insert into PR_PREDIAL_CARACTERISTICAS (SERIE,IDENTIFICADOR,NOMBRE,ETIQUETA,TIPO_DE_DATO,LONGITUD_MAXIMA,LONGITUD_DESPLIEGUE,TIPO_DE_VALIDACION,REQUERIDA,ESTATUS,ORDEN,COMPORTAMIENTO,NIVEL,PR_TIAV_SERIE,PR_TIAV_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (9999,999999999999999,'NOMBRE','ETIQUETA','CA',12345678910,12345678910,'AL','S','AC',1,'A','1',9999,999999999999999,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'RECAUDADOR',to_timestamp('20/09/11','DD/MM/RR HH24:MI:SSXFF'),'RECAUDADOR',to_timestamp('20/09/11','DD/MM/RR HH24:MI:SSXFF'))");
            stmt.execute("Insert into PR_PRED_BLOQUES_AGRUPACIONES (IDENTIFICADOR,NOMBRE,ETIQUETA,ORDEN,PANTALLA,PR_PRBA_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (999999999999999,'NOMBRE','ETIQUETA',123456789,123456789,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'RECAUDADOR',to_timestamp('09/09/11','DD/MM/RR HH24:MI:SSXFF'),'RECAUDADOR',to_timestamp('09/09/11','DD/MM/RR HH24:MI:SSXFF'))");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }

    /**
     * M�todo que mediante sentencias de SQL borra todo lo creado para las
     * pruebas de la vista.
     *
     * @author Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    @AfterClass
    public static void tearDown() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("delete from pr_pred_bloques_agrupaciones where identificador = 999999999999999");
            stmt.execute("delete from pr_predial_caracteristicas where serie = 9999 and identificador = 999999999999999");
            stmt.execute("delete from pr_tipos_avaluos where identificador = 999999999999999");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }
}

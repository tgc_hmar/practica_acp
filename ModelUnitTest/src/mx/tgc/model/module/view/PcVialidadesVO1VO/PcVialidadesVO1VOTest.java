package mx.tgc.model.module.view.PcVialidadesVO1VO;

import java.sql.SQLException;
import java.sql.Statement;

import mx.net.tgc.util.FechasUtil;

import mx.tgc.model.module.CataCaractAsentamientoAMImpl;
import mx.tgc.model.module.applicationModule.CataCaractAsentamientoAMFixture;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PcVialidadesVO1VOTest {
    private static CataCaractAsentamientoAMFixture fixture1 =
        CataCaractAsentamientoAMFixture.getInstance();
    private static CataCaractAsentamientoAMImpl _amImpl =
        (CataCaractAsentamientoAMImpl)fixture1.getApplicationModule();

    public PcVialidadesVO1VOTest() {
    }

    /**
     * M�todo que sirve para probar la creaci�n de la instancia de la vista.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testCreacionInstancia() {
        ViewObject view = _amImpl.findViewObject("PcVialidadesVO1");
        assertNotNull(view);
    }

    /**
     * M�todo que sirve para probar los campos obligatorios
     * verifica que efectivamente si no se llenan los campos obligatorios
     * al validar el nuevo registro debe arrojar error.
     *
     * @author: Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testCamposObligatorios() {
        ViewObject view = _amImpl.findViewObject("PcVialidadesVO1");
        Row row = view.createRow();

        //TODO: por cada atributo obligatorio en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Identificador", 999999999999999L);
        row.setAttribute("ClaveVialidad", "CLAVEVIAL");
        row.setAttribute("TipoVialidad", "TIPOVIALIDAD");
        row.setAttribute("NombreVialidad", "NOMBREVIALIDAD");
        row.setAttribute("FechaAlta", FechasUtil.fechaDeHoy());
        row.setAttribute("PcLocaIdentificador", 999999999999999L);
        //-------------
        row.validate();
        _amImpl.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar la longitud de los campos
     * de una vista de datos, el m�todo prueba que las longitudes
     * sean probadas con la m�xima longitud permitida para los
     * campos de tipo String (VARCHAR2).
     *
     * @author: Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testPresicionMaximaEnCampos() {
        ViewObject view = _amImpl.findViewObject("PcVialidadesVO1");
        Row row = view.createRow();

        //TODO: variables predefinidas con varias longitudes para
        //probar el m�ximo de caracteres permitidos en los campos de las tablas
        String campo2000Chars = "";
        for (int i = 0; i < 2000; i++) {
            campo2000Chars += "1";
        }

        String campo300Chars = "";
        for (int i = 0; i < 300; i++) {
            campo300Chars += "1";
        }

        String campo100Chars = "";
        for (int i = 0; i < 100; i++) {
            campo100Chars += "1";
        }

        String campo30Chars = "";
        for (int i = 0; i < 30; i++) {
            campo30Chars += "1";
        }

        String campo20Chars = "";
        for (int i = 0; i < 20; i++) {
            campo20Chars += "1";
        }

        String campo15Chars = "";
        for (int i = 0; i < 15; i++) {
            campo15Chars += "1";
        }

        String campo10Chars = "";
        for (int i = 0; i < 10; i++) {
            campo10Chars += "1";
        }

        String campo4Chars = "";
        for (int i = 0; i < 4; i++) {
            campo4Chars += "1";
        }

        String campo2Chars = "";
        for (int i = 0; i < 2; i++) {
            campo2Chars += "1";
        }

        //TODO: por cada atributo en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Identificador", Long.parseLong(campo15Chars));
        row.setAttribute("ClaveVialidad", campo10Chars);
        row.setAttribute("TipoVialidad", campo15Chars);
        row.setAttribute("NombreVialidad", campo300Chars);
        row.setAttribute("NombreCorto", campo100Chars);
        row.setAttribute("FechaAlta", FechasUtil.fechaDeHoy());
        row.setAttribute("Estatus", campo2Chars);
        row.setAttribute("PcLocaIdentificador", 999999999999999L);
        row.setAttribute("Campo1", campo2000Chars);
        row.setAttribute("Campo2", campo2000Chars);
        row.setAttribute("Campo3", campo2000Chars);
        row.setAttribute("Campo4", campo2000Chars);
        row.setAttribute("Campo5", campo2000Chars);
        row.setAttribute("Campo6", campo2000Chars);
        row.setAttribute("Campo7", campo2000Chars);
        row.setAttribute("Campo8", campo2000Chars);
        row.setAttribute("Campo9", campo2000Chars);
        row.setAttribute("Campo10", campo2000Chars);
        //-------------------
        row.validate();
        _amImpl.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar altas y
     * bajas de registros en la vista de datos
     * se crea un nuevo row al cual se le llena con datos
     * de prueba para poder probar las operaciones de
     * insert y delete.
     *
     * @author: Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testInsertUpdateDelete() {
        ViewObject view = _amImpl.findViewObject("PcVialidadesVO1");
        Row row = view.createRow();

        //TODO: Llenar cada atributo de la vista con valores para probar los inserts y deletes
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Identificador", 999999999999999L);
        row.setAttribute("ClaveVialidad", "CLAVEVIAL");
        row.setAttribute("TipoVialidad", "TIPOVIALIDAD");
        row.setAttribute("NombreVialidad", "NOMBREVIALIDAD");
        row.setAttribute("FechaAlta", FechasUtil.fechaDeHoy());
        row.setAttribute("PcLocaIdentificador", 999999999999999L);
        _amImpl.getDBTransaction().commit();
        //-------------------
        row.setAttribute("Estatus", "AC");
        _amImpl.getDBTransaction().commit();
        //-------------------
        row.remove();
        _amImpl.getDBTransaction().commit();
    }

    /**
     * M�todo que mediante sentencias de SQL inserta registros en diversas
     * tablas, necesarios para iniciar las pruebas de la vista.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @BeforeClass
    public static void setUp() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("Insert into PC_ESTADOS (IDENTIFICADOR,CLAVE_ESTADO,DESCRIPCION,NOMBRE_CORTO,CAPITAL,TOTAL_MUNICIPIOS,ESTATUS,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5) values (999999999999999,'EDO','ESTADO_PRUEBA','EDO',null,1,'AC',null,null,null,null,null)");
            stmt.execute("Insert into PC_MUNICIPIOS (IDENTIFICADOR,PC_ESTA_IDENTIFICADOR,CLAVE_MUNICIPIO,DESCRIPCION,NOMBRE_CORTO,CABECERA_MUNICIPAL,ESTATUS,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5) values (999999999999999,999999999999999,'MUN','MUNICIPIO_PRUEBA','MUNI',null,'AC',null,null,null,null,null)");
            stmt.execute("Insert into PC_LOCALIDADES (IDENTIFICADOR,PC_MUNI_IDENTIFICADOR,CLAVE_LOCALIDAD,DESCRIPCION,SINONIMO,ESTATUS,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5) values (999999999999999,999999999999999,'LOC','LOCALIDAD_PRUEBA','LOC','AC',null,null,null,null,null)");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }

    /**
     * M�todo que mediante sentencias de SQL borra todo lo creado para las
     * pruebas de la vista.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @AfterClass
    public static void tearDown() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("delete from pc_localidades where identificador = 999999999999999");
            stmt.execute("delete from pc_municipios where identificador = 999999999999999");
            stmt.execute("delete from pc_estados where identificador = 999999999999999");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }
}

/**
  * CatalogoSencilloAMTest
  * 
  * 17/12/2013
  * Componentes: CatalogoSencilloAM
  *
  * Tecnolog�a de Gesti�n y Comunicaci�n SA de CV
  * Desarrollo de Aplicaciones
  *
  * Todos los Derechos Reservados
  * � Copyright 2013
  * http://www.tgc.mx/
  */


package mx.tgc.model.module.applicationModule;


import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import mx.net.tgc.recaudador.resource.pc.Accion;
import mx.net.tgc.recaudador.resource.pr.Elemento;
import mx.net.tgc.recaudador.resource.pr.TipoMovimientoPredio;

import mx.tgc.model.module.CatalogoSencilloAMImpl;

import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;


public class CatalogoSencilloAMTest {
    private static CatalogoSencilloAMFixture fixture1 =
        CatalogoSencilloAMFixture.getInstance();
    private static CatalogoSencilloAMImpl _amImpl =
        (CatalogoSencilloAMImpl)fixture1.getApplicationModule();


    public static final Map datosAuditoria;
    static {
        datosAuditoria = new HashMap();
        datosAuditoria.put("userName", "JU");
        datosAuditoria.put("ipAddress", "127.0.0.1");
        datosAuditoria.put("host", "host");
    }

    public CatalogoSencilloAMTest() {

    }

    /*
     * Preparaci�n para las pruebas
     */

    /**
    * Metodo al que entra antes de realizar cualquier prueba, se manda
    * llamar a los metodos para insertar los registros necesarios para
    * las demas pruebas
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */

    @BeforeClass
    public static void setUp() {
        try {
            insertUsuario();
            insertTiposMovimientosPredios();
            insertTiposMovimientosPrediosUsuario();
            insertElementosAcciones();
            insertElementosAccionesConfiguracion();
            _amImpl.getDBTransaction().commit();
        } catch (Exception ex) {
            _amImpl.getDBTransaction().rollback();
            ex.printStackTrace();
        }
    }

    /**
    * Inserta el registro de Usuario JU en AA_Usuarios
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void insertUsuario() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("INSERT INTO AA_USUARIOS (USUARIO,NOMBRE,PUESTO,NUMERO_EMPLEADO,PASSWORD,PREGUNTA,RESPUESTA,ESTATUS,TIPO_USUARIO,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5) VALUES ('JU','USUARIO JUNIT','RECAUDADOR ESTATAL',NULL,'JU',NULL,NULL,'AC','L',NULL,'Usuarios',NULL,NULL,NULL)");
    }

    /**
    * Inserta los Elementos y Acciones en PR_Acciones y PR_Elementos
    * con llaves 9999-1 y 9999-2
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void insertElementosAcciones() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("INSERT INTO PR_ELEMENTOS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,DOMINIO,ETIQUETA,COMPORTAMIENTO,FORMULA_VALIDACION,NIVEL,PR_ELEM_SERIE,PR_ELEM_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) VALUES (9999,1,'ELE1','ELEMENTO 1','PRIMER ELEMENTO',NULL,'PRIMER ELEMENTO',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'JU',TO_DATE('15/09/99','DD/MM/RR'),'JU',TO_DATE('15/09/99','DD/MM/RR'))");
        stmt.execute("Insert into PR_ELEMENTOS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,DOMINIO,ETIQUETA,COMPORTAMIENTO,FORMULA_VALIDACION,NIVEL,PR_ELEM_SERIE,PR_ELEM_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (9999,2,'ELE2','ELEMENTO 2','SEGUNDO ELEMENTO',null,'SEGUNDO ELEMENTO',null,null,1,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'JU',to_date('15/09/99','DD/MM/RR'),'JU',to_date('15/09/99','DD/MM/RR'))");
        stmt.execute("INSERT INTO PR_ACCIONES (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,ETIQUETA,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) VALUES (9999,1,'JUACC1','FOLIO_SERIE','Seccion de folio y serie','FOLIO Y SERIE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'JU',TO_DATE('19/12/11','DD/MM/RR'),'JU',TO_DATE('19/12/11','DD/MM/RR'))");
        stmt.execute("Insert into PR_ACCIONES (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,ETIQUETA,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (9999,2,'JUACC2','GENERA_AVALUO','GENERA AVALUO','GENERA AVALUO',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'JU',to_date('19/12/11','DD/MM/RR'),'JU',to_date('19/12/11','DD/MM/RR'))");
    }

    /**
    * Inserta las configuraciones de elementos y acciones
    * PR_Configuraciones_Elementos y PR_Configuraciones_Acciones
    * al tipo movimiento 9999-1
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void insertElementosAccionesConfiguracion() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("Insert into PR_CONFIGURACIONES_ACCIONES (PR_TMPR_SERIE,PR_TMPR_IDENTIFICADOR,PR_ACCI_SERIE,PR_ACCI_IDENTIFICADOR,ORDEN,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (9999,1,9999,1,1,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'JU',to_date('17/12/13','DD/MM/RR'),'JU',to_date('17/12/13','DD/MM/RR'))");
        stmt.execute("INSERT INTO PR_CONFIGURACIONES_ELEMENTOS (PR_TMPR_SERIE,PR_TMPR_IDENTIFICADOR,PR_ELEM_SERIE,PR_ELEM_IDENTIFICADOR,ORDEN,MODIFICABLE,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) VALUES (9999,1,9999,1,1,'S',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'JU',TO_DATE('17/12/13','DD/MM/RR'),'JU',TO_DATE('17/12/13','DD/MM/RR'))");
    }

    /**
    * Inserta los tipos movimientos predios 
    * PR_Tipos_Movimientos_Predios con llaves 9999-1 y 9999-2
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void insertTiposMovimientosPredios() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("INSERT INTO PR_TIPOS_MOVIMIENTOS_PREDIOS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,MODULO,ESTATUS,GENERACOBRO,PROCESOCOBRO,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) VALUES (9999,1,'TMJU01','APLICAR ADEUDO AL PREDIO','TIPO MOVIMIENTO JU 01','APLICACION','AC','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'JU',TO_DATE('17/01/12','DD/MM/RR'),'JU',TO_DATE('17/01/12','DD/MM/RR'))");
        stmt.execute("INSERT INTO PR_TIPOS_MOVIMIENTOS_PREDIOS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,MODULO,ESTATUS,GENERACOBRO,PROCESOCOBRO,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) VALUES (9999,2,'TMJU02','APLICAR DESCUENTO AL PREDIO','TIPO MOVIMIENTO JU 02','APLICACION','AC','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'JU',TO_DATE('17/01/12','DD/MM/RR'),'JU',TO_DATE('17/01/12','DD/MM/RR'))");
    }

    /**
    * Inserta los movimientos predios usuario al usuario JU 
    * PR_Tipos_Movs_Predios_Usuarios
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void insertTiposMovimientosPrediosUsuario() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("INSERT INTO PR_TIPOS_MOVS_PREDIOS_USUARIOS (AA_USUA_USUARIO,PR_TMPR_SERIE,PR_TMPR_IDENTIFICADOR) VALUES ('JU',9999,1)");
    }

    @AfterClass
    /**
    * Metodo al que entra despues de haber ejecutado las pruebas, se encarga
    * de llamar a los metodos de eliminar los registros creados para las prubeas
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    public static void tearDown() {
        try {
            deleteElementosAccionesConf();
            deleteElementosAcciones();
            deleteTiposMovimientosUsuarios();
            deleteTiposMovimientos();
            deleteUsuario();
            _amImpl.getDBTransaction().commit();
        } catch (Exception ex) {
            _amImpl.getDBTransaction().rollback();
            ex.printStackTrace();
        }
    }

    /**
    * Borra el usuario JU AA_Usuarios y las auditorias creadas 
    * por sus movimientos AA_Auditorias
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void deleteUsuario() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("DELETE FROM AA_AUDITORIAS WHERE AA_USUA_USUARIO = 'JU'");
        stmt.execute("DELETE FROM AA_USUARIOS WHERE USUARIO = 'JU'");
    }
    
    /**
    * Elimina la configuracion de acciones y elementos al tipo movmimiento
    * con llave 9999-1
    * PR_Configuraciones_Elementos y PR_Configuraciones_Acciones
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void deleteElementosAccionesConf() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("DELETE FROM PR_CONFIGURACIONES_ACCIONES WHERE PR_TMPR_IDENTIFICADOR = 1 AND PR_TMPR_SERIE = 9999");
        stmt.execute("DELETE FROM PR_CONFIGURACIONES_ELEMENTOS WHERE PR_TMPR_IDENTIFICADOR = 1 AND PR_TMPR_SERIE = 9999");
    }

    /**
    * Elimina los elementos y acciones con llaves 9999-1 y 9999-2
    * PR_Acciones PR_Elementos
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void deleteElementosAcciones() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("DELETE FROM PR_ELEMENTOS WHERE IDENTIFICADOR = 1 AND SERIE = 9999");
        stmt.execute("DELETE FROM PR_ELEMENTOS WHERE IDENTIFICADOR = 2 AND SERIE = 9999");
        stmt.execute("DELETE FROM PR_ACCIONES WHERE IDENTIFICADOR = 1 AND SERIE = 9999");
        stmt.execute("DELETE FROM PR_ACCIONES WHERE IDENTIFICADOR = 2 AND SERIE = 9999");

    }

    /**
    * Elimina los tipos movimientos usuarios del usuario JU
    * PR_Tipos_Movs_Predios_Usuarios
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void deleteTiposMovimientosUsuarios() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("DELETE FROM PR_TIPOS_MOVS_PREDIOS_USUARIOS WHERE AA_USUA_USUARIO = 'JU'");
    }

    /**
    * Elimina los tipos movimientos con llaves 9999-1 9999-2
    * PR_TIPOS_MOVIMIENTOS_PREDIOS
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    private static void deleteTiposMovimientos() throws SQLException {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        stmt.execute("DELETE FROM PR_TIPOS_MOVIMIENTOS_PREDIOS WHERE SERIE = 9999 AND IDENTIFICADOR = 1");
        stmt.execute("DELETE FROM PR_TIPOS_MOVIMIENTOS_PREDIOS WHERE SERIE = 9999 AND IDENTIFICADOR = 2");
    }

    /*
     * M�todos a probar
     */

    /**
    * Prueba que el metodo getAllAcciones devuelva una lista de
    * Acciones registradas en PR_Acciones
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    @Test
    public void testGetAllAcciones() {
        List<Accion> lista = _amImpl.getAllAcciones();
        boolean empty = false;
        if (lista == null || lista.isEmpty()) {
            empty = true;
        }
        assertFalse("Lista de acciones no devuelve resultados", empty);
    }

    /**
    * Prueba que el metodo getElementos devuelva una lista de
    * Elementos registrados en PR_Elementos
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    @Test
    public void testGetAllElementos() {
        List<Elemento> lista = _amImpl.getAllElementos();
        boolean empty = false;
        if (lista == null || lista.isEmpty()) {
            empty = true;
        }
        assertFalse("Lista de elementos no devuelve resultados", empty);
    }

    /**
    * Prueba que el metodo getAllTiposMovimientos devuelva una lista de
    * TiposMovimientosPredio registrados en Pr_Tipos_Movimientos_Predios
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    @Test
    public void testGetAllTiposMovimientos() {
        List<TipoMovimientoPredio> lista =
            _amImpl.getAllTiposMovimientosPredios();
        boolean empty = false;
        if (lista == null || lista.isEmpty()) {
            empty = true;
        }
        assertFalse("Lista de tipos movimientos predios no devuelve resultados",
                    empty);
    }

    /**
    * Prueba que el metodo getTiposMovimientosAsignados el cual regresa
    * una lista de TiposMovimientosPredios asignados a un usuario.
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    @Test
    public void testGetTiposMovimientosAsignados() {
        List<TipoMovimientoPredio> lista =
            _amImpl.getTiposMovimientosAsignados("JU");
        boolean empty = false;
        if (lista == null || lista.isEmpty()) {
            empty = true;
        }
        assertFalse("Lista de tipos movimientos asignados para JU vacio",
                    empty);
    }

    /**
    * Prueba que el metodo getElementosAsignados el cual regresa
    * una lista de Elementos asignados a un tipo movimiento predio.
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    @Test
    public void testGetElementosAsignados() {
        Integer serie = 9999;
        Long id = 1L;
        List<Elemento> lista = _amImpl.getElementosAsignados(serie, id);
        boolean empty = false;
        if (lista == null || lista.isEmpty()) {
            empty = true;
        }
        assertFalse("Lista de elementos no devuelve resultados", empty);
    }

    /**
    * Prueba que el metodo getAccionesAsignadas el cual regresa
    * una lista de Acciones asignadas a un tipo movimiento predio.
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    @Test
    public void testGetAccionesAsignadas() {
        Integer serie = 9999;
        Long id = 1L;
        List<Accion> lista = _amImpl.getAccionesAsignadas(serie, id);
        boolean empty = false;
        if (lista == null || lista.isEmpty()) {
            empty = true;
        }
        assertFalse("Lista de acciones no devuelve resultados", empty);
    }

    /**
    * Prueba que el metodo insertaAccionesMovimientos el cual inserta
    * las acciones a un tipo movmimiento predio.
    * Arma 
    * accionesAsignadas, acciones nuevas
    * accionesAnteriores, acciones que tenia antes de la modificacion
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    @Test
    public void testInsertaAccionesMovimientos() {
        boolean exito = false;
        try {
            Accion old = _amImpl.getAccion(9999, 1L);

            List<String> accionesAsignadas = new ArrayList<String>();
            accionesAsignadas.add("9999-2");

            List<Accion> accionesAnteriores = new ArrayList<Accion>();
            accionesAnteriores.add(old);

            Integer serie = 9999;
            Long id = 1L;

            _amImpl.insertaAccionesMovimientos(accionesAsignadas,
                                               accionesAnteriores,
                                               datosAuditoria, serie, id);
            exito = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        assertTrue("Error al insertar las acciones para el movimiento", exito);
    }

    /**
    * Prueba que el metodo insertaElementosMovimientos el cual inserta
    * los elementos a un tipo movmimiento predio.
    * Arma 
    * eleAsignados, elementos nuevos
    * eleAnteriores, elementos que tenia antes de la modificacion
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    @Test
    public void testInsertaElementosMovimientos() {
        boolean exito = false;
        try {
            Elemento old = _amImpl.getElemento(9999, 1L);

            List<String> eleAsignados = new ArrayList<String>();
            eleAsignados.add("9999-2");

            List<Elemento> eleAnteriores = new ArrayList<Elemento>();
            eleAnteriores.add(old);

            Integer serie = 9999;
            Long id = 1L;

            _amImpl.insertaElementosMovimientos(eleAsignados, eleAnteriores,
                                                datosAuditoria, serie, id);
            exito = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        assertTrue("Error al insertar los elementos para el movimiento",
                   exito);
    }

    /**
    * Prueba que el metodo insertaTiposMovPredioUsuario el cual inserta
    * los tipos movimientos predio a un usaurio.
    * Arma 
    * movAsignaddos, tipos movimientos nuevos
    * movAnteriores, tipos movimientos que tenia antes de la modificacion
    * 
    * @author Oscar Porres
    * @versi�n 1.0 17/12/2013
    * @component CatalogoSencillo
    *
    */
    @Test
    public void testInsertaTiposMovPrediosUsuario() {
        boolean exito = false;
        try {
            TipoMovimientoPredio old =
                _amImpl.getTipoMovimientoPredio(9999, 1L);

            List<String> movAsignados = new ArrayList<String>();
            movAsignados.add("9999-2");

            List<TipoMovimientoPredio> movAnteriores =
                new ArrayList<TipoMovimientoPredio>();
            movAnteriores.add(old);

            _amImpl.insertaTiposMovPrediosUsuario(movAsignados, movAnteriores,
                                                  datosAuditoria);
            exito = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        assertTrue("Error al insertar los tipos movimientos para el usuario",
                   exito);
    }

}

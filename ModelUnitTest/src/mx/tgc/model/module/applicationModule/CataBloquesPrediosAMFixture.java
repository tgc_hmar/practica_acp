package mx.tgc.model.module.applicationModule;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

public class CataBloquesPrediosAMFixture {
    private static CataBloquesPrediosAMFixture fixture1 =
        new CataBloquesPrediosAMFixture();
    private ApplicationModule _am;

    private CataBloquesPrediosAMFixture() {
        _am =
Configuration.createRootApplicationModule("mx.tgc.model.module.CataBloquesPrediosAM",
                                          "CataBloquesPrediosAMLocal");
    }

    public void setUp() {
    }

    public void tearDown() {
    }

    public static CataBloquesPrediosAMFixture getInstance() {
        return fixture1;
    }

    public void release() throws Exception {
        Configuration.releaseRootApplicationModule(_am, true);
    }

    public ApplicationModule getApplicationModule() {
        return _am;
    }
}

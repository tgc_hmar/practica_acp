package mx.tgc.model.module.applicationModule;

import java.sql.SQLException;
import java.sql.Statement;

import mx.tgc.model.module.CataSegCatastralesAMImpl;
import mx.tgc.model.view.ubicacion.PrSegmentosCatastralesUbicaVOImpl;
import mx.tgc.model.view.catastro.PrSegmentosCatastralesVOImpl;

import oracle.jbo.domain.Number;

import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

public class CataSegCatastralesAMTest {
  private static CataSegCatastralesAMFixture fixture1 =
    CataSegCatastralesAMFixture.getInstance();
  private static CataSegCatastralesAMImpl _amImpl =
    (CataSegCatastralesAMImpl)fixture1.getApplicationModule();

  public CataSegCatastralesAMTest() {
  }

  @BeforeClass
  public static void setUp() {
    insertSegmentoCatastral();
  }

  @AfterClass
  public static void tearDown() {
    deleteSegmentoCatastral();
  }

  /**
   * M�todo que sirve para crear los registros de prueba
   * por medio de un insert a la tabla de PrSegmentosCatastrales
   * y otro a la de PrSegmentosCatastralesUbica.
   *
   * @author: Victor Alejandro Venegas Villalobos
   * @since 20-SEP-2011
   */
  private static void insertSegmentoCatastral() {
    Statement stmt = _amImpl.getDBTransaction().createStatement(1);
    try {
      stmt.executeUpdate("Insert Into Pr_Segmentos_Catastrales (Serie,Identificador,Clave_Segmento,Descripcion,Tipo_Segmento,Nivel,Estatus,Cc_Reca_Identificador,Pr_Seca_Serie,Pr_Seca_Identificador,Campo1,Campo2,Campo3,Campo4,Campo5,Campo6,Campo7,Campo8,Campo9,Campo10,Campo11,Campo12,Campo13,Campo14,Campo15,Creado_Por,Creado_El,Modificado_Por,Modificado_El) values (2100,77777,'00000','JUNIT PRUEBA','MU',1,'AC',1,NULL,NULL,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'00000','RECAUDADOR',to_date('15/09/11','DD/MM/RR'),'RECAUDADOR',to_date('19/09/11','DD/MM/RR'))");
      stmt.executeUpdate("Insert Into Pr_Segmentos_Catastrales_Ubica (Serie,Identificador,Pr_Seca_Serie,Pr_Seca_Identificador,Estatus,Pc_Asen_Identificador,Pc_Loca_Identificador,Pc_Vial_Identificador,Campo1,Campo2,Campo3,Campo4,Campo5,Campo6,Campo7,Campo8,Campo9,Campo10,Campo11,Campo12,Campo13,Campo14,Campo15,Creado_Por,Creado_El,Modificado_Por,Modificado_El) Values (2011,14,2100,77777,'AC',Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,'RECAUDADOR',To_Date('14/09/11','DD/MM/RR'),'RECAUDADOR',To_Date('14/09/11','DD/MM/RR'))");
      stmt.executeUpdate("Insert Into Pr_Segmentos_Catastrales (Serie,Identificador,Clave_Segmento,Descripcion,Tipo_Segmento,Nivel,Estatus,Cc_Reca_Identificador,Pr_Seca_Serie,Pr_Seca_Identificador,Campo1,Campo2,Campo3,Campo4,Campo5,Campo6,Campo7,Campo8,Campo9,Campo10,Campo11,Campo12,Campo13,Campo14,Campo15,Creado_Por,Creado_El,Modificado_Por,Modificado_El) Values (2100,88888,'11111','JUNIT PRUEBA2','SE',1,'AC',1,2100,77777,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,'00000-11111','RECAUDADOR',To_Date('15/09/11','DD/MM/RR'),'RECAUDADOR',To_Date('19/09/11','DD/MM/RR'))");
      stmt.executeUpdate("Insert Into Pr_Segmentos_Catastrales (Serie,Identificador,Clave_Segmento,Descripcion,Tipo_Segmento,Nivel,Estatus,Cc_Reca_Identificador,Pr_Seca_Serie,Pr_Seca_Identificador,Campo1,Campo2,Campo3,Campo4,Campo5,Campo6,Campo7,Campo8,Campo9,Campo10,Campo11,Campo12,Campo13,Campo14,Campo15,Creado_Por,Creado_El,Modificado_Por,Modificado_El) Values (2100,99999,'22222','JUNIT PRUEBA3','SE',1,'AC',1,2100,77777,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,'00000-22222','RECAUDADOR',To_Date('15/09/11','DD/MM/RR'),'RECAUDADOR',To_Date('19/09/11','DD/MM/RR'))");
      _amImpl.getDBTransaction().commit();
    } catch (SQLException ex) {
      ex.printStackTrace();
      _amImpl.getDBTransaction().rollback();
    }
  }

  /**
   * M�todo que sirve para saber si alg�n registro de prueba
   * previamente insertado tiene alguna ubicaci�n asignada.
   *
   * @author: Victor Alejandro Venegas Villalobos
   * @since 20-SEP-2011
   */
  @Test
  public void testFiltraSegCataUbica() {
    _amImpl.filtraSegCataUbica(new Number(2100), new Number(77777));
    PrSegmentosCatastralesUbicaVOImpl segmentosUbicaVO1 =
      _amImpl.getPrSegmentosCatastralesUbicaVO1();
    int cantidadSegmentos = segmentosUbicaVO1.getRowCount();
    assertTrue("La cantidad esperada de segmentos ubicacion es 1 y se recibieron: " +
               cantidadSegmentos, cantidadSegmentos == 1);
  }

  @Test
  public void testFiltrarPorRecaudacion() {
  }

  /**
   * M�todo que sirve para probar que los registros insertados
   * previamente sean segmentos catastrales v�lidos.
   *
   * @author: Victor Alejandro Venegas Villalobos
   * @since 20-SEP-2011
   */
  @Test
  public void testFiltraSegCatastrales() {
    _amImpl.filtraSegCatastrales(new Number(2100), new Number(77777));
    PrSegmentosCatastralesVOImpl segmentosCatastralesVO2 =
      _amImpl.getPrSegmentosCatastralesVO2();
    int cantidadSegmentos = segmentosCatastralesVO2.getRowCount();
    assertTrue("La cantidad esperada de segmentos es 2 y se recibieron: " +
               cantidadSegmentos, cantidadSegmentos == 2);
  }

  /**
   * M�todo que sirve para verificar que antes de borrar alg�n registro
   * pase las validaciones previamente realizadas.
   *
   * @author: Victor Alejandro Venegas Villalobos
   * @since 20-SEP-2011
   */
  @Test
  public void testValidaAntesDeBorrar() {
    String mensaje = _amImpl.validaAntesDeBorrar(2100, 77777);
    assertTrue("La respuesta esperada es un mensaje de error y se recibio: " +
               mensaje, mensaje != "OK");
  }

  /**
   * M�todo que sirve para verificar que antes de editar alg�n registro
   * pase las validaciones previamente realizadas.
   *
   * @author: Victor Alejandro Venegas Villalobos
   * @since 20-SEP-2011
   */
  @Test
  public void testValidaAntesDeEditar() {
    String mensaje = _amImpl.validaAntesDeEditar(2100, 77777);
    assertTrue("La respuesta esperada es un mensaje de error y se recibio: " +
               mensaje, mensaje != "OK");
  }

  @Test
  public void testValidaAntesDeInsertar() {
  }

  @Test
  public void testBeforeCommit() {
  }

  /**
   * M�todo que sirve para validar la clave del segmento.
   *
   * @author: Victor Alejandro Venegas Villalobos
   * @since 20-SEP-2011
   */
  @Test
  public void testValidarClave() {
    Boolean mensaje = _amImpl.validarClave("77777");
    assertTrue("Si es valida la clave se regresa un true y se regres�: " +
               mensaje, mensaje == true);
  }

  /**
   * M�todo que sirve para eliminar por medio del delete
   * los registros anteriormente creados para la prueba.
   *
   * @author: Victor Alejandro Venegas Villalobos
   * @since 20-SEP-2011
   */
  private static void deleteSegmentoCatastral() {
    Statement stmt = _amImpl.getDBTransaction().createStatement(1);
    try {
      stmt.execute("Delete from Pr_Segmentos_catastrales_Ubica where Serie = 2011 and Identificador = 14");
      stmt.execute("Delete from Pr_Segmentos_Catastrales where Serie = 2100 and Identificador in (77777,88888,99999)");
      _amImpl.getDBTransaction().commit();
    } catch (SQLException ex) {
      ex.printStackTrace();
      _amImpl.getDBTransaction().rollback();
    }
  }
}

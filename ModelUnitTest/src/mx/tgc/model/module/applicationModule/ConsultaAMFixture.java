package mx.tgc.model.module.applicationModule;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

public class ConsultaAMFixture {
    private static ConsultaAMFixture fixture1 = 
        new ConsultaAMFixture();
    private ApplicationModule _am;

    private ConsultaAMFixture() {
        _am =
Configuration.createRootApplicationModule("mx.tgc.model.module.ConsultaAM",
                                          "ConsultaAMLocal");
    }

    public void setUp() {
    }

    public void tearDown() {
    }

    public static ConsultaAMFixture getInstance() {
        return fixture1;
    }

    public void release() throws Exception {
        Configuration.releaseRootApplicationModule(_am, true);
    }

    public ApplicationModule getApplicationModule() {
        return _am;
    }
}

package mx.tgc.model.module;

import mx.tgc.model.module.applicationModule.CatalogoSencilloAMFixture;
import mx.tgc.model.module.applicationModule.CatalogoSencilloAMTest;
import mx.tgc.model.module.view.PrAccionesVVO1VO.PrAccionesVVO1VOTest;
import mx.tgc.model.module.view.PrConfiguracionesAccionesVO1VO.PrConfiguracionesAccionesVO1VOTest;
import mx.tgc.model.module.view.PrConfiguracionesElementosVO1VO.PrConfiguracionesElementosVO1VOTest;
import mx.tgc.model.module.view.PrElementosVVO1VO.PrElementosVVO1VOTest;
import mx.tgc.model.module.view.PrTiposAvaluos1VO.PrTiposAvaluos1VOTest;
import mx.tgc.model.module.view.PrTiposMovimientosPredios1VO.PrTiposMovimientosPredios1VOTest;
import mx.tgc.model.module.view.PrTiposMovimientosPrediosVVO1VO.PrTiposMovimientosPrediosVVO1VOTest;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { PrTiposMovimientosPrediosVVO1VOTest.class,
                       PrConfiguracionesElementosVO1VOTest.class,
                       PrConfiguracionesAccionesVO1VOTest.class,
                       PrElementosVVO1VOTest.class, PrAccionesVVO1VOTest.class,
                       PrTiposMovimientosPredios1VOTest.class,
                       PrTiposAvaluos1VOTest.class,
                       CatalogoSencilloAMTest.class })
public class AllCatalogoSencilloAMTests {
    @BeforeClass
    public static void setUp() {
    }

    @AfterClass
    public static void tearDown() throws Exception {
        CatalogoSencilloAMFixture.getInstance().release();
    }
}

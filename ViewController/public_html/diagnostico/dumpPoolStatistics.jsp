<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="
java.io.*,
java.util.Enumeration,
java.util.StringTokenizer,
javax.servlet.ServletConfig,
javax.servlet.ServletException,
javax.servlet.http.HttpServlet,
javax.servlet.http.HttpServletRequest,
javax.servlet.http.HttpServletResponse,
oracle.jbo.common.ampool.ApplicationPool,
oracle.jbo.common.ampool.PoolMgr"%>
<html>
    <head><title>dumpPoolStatistics</title></head><%
    PoolMgr poolMgr = PoolMgr.getInstance(); 
    String poolname = request.getParameter("poolname");
    if (poolname == null || poolname.equals("")) {
      Enumeration keys = poolMgr.getPoolNames();
      if (keys != null) {
        out.println("<h3>List of Active Application Module Pools</h3>");
        out.println("<ul>");
        while (keys.hasMoreElements()) {
          String s = (String)keys.nextElement();
          out.println("<li><code><a href='dumpPoolStatistics.jsp?poolname="+
          s+"'>"+s+"</a></code></li>");
        }
        out.println("</ul>");
      }
      else {
        out.println("No pools.");
      }
    }
    else {
      out.println("<h3>Pool Statistics for Pool '"+poolname+"'</h3>");
      out.println("<a href='dumpPoolStatistics.jsp'>Back to Pool List</a>");
      out.println("<hr><blockquote><pre>");
      ApplicationPool pool =  (ApplicationPool)poolMgr.getResourcePool(poolname);
      StringWriter stringWriter = new StringWriter();  
      PrintWriter writer = new PrintWriter(stringWriter);  
      pool.dumpPoolStatistics(writer);
      String dump = stringWriter.toString();
      String[] lines = dump.split(System.getProperty("line.separator"));
      String line;
      String titulo;
      String parametro;
      String valor;
     
      for (int i = 0; i < lines.length; i++)  {
        line = lines[i];
        if (line.startsWith("head:")) {
           titulo = line.substring(5, line.length());
           out.println("<br><b>" + titulo + "</b>");
        }
        else {
            StringTokenizer st = new StringTokenizer(line, ",");
            parametro = st.nextToken();
            valor = st.nextToken();
            out.println("- " + parametro + "= " + "<b>" + valor + "</b>");
        }
      }
      
      out.println("</pre></blockquote>");
    }
%> 
 
package mx.tgc.view.backing.catalogo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;

import javax.el.ExpressionFactory;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.resource.RowTreeTable;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class CataSegCatastralesBean extends GenericBean {
    private RichDialog cancelarTransaccion;
    private RichSelectOneChoice secaPadre;
    private RichSelectOneChoice municipios;
    private RichPopup popupDml;
    PropertiesReader properties;
    PropertiesReader propertiesModel;
    private RichInputText nivelSegmento;
    private String accion;
    private RichPopup popupCancelar;

    public CataSegCatastralesBean() {
        if (properties == null) {
            try {
                properties = new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties", CataSegCatastralesBean.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (propertiesModel == null) {
            try {
                propertiesModel = new PropertiesReader("/mx/tgc/model/ModelBundle.properties", CataSegCatastralesBean.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Este m�todo determina el nombre de los atributos por los cuales se van
     * a poder realizar las b�squedas. Estos atributos se guardan en una lista de
     * objetos SelectItem. Tambi�n contiene la especificaci�n del Managed Bean que
     * corresponde a este cat�logo. Finalmente se a�aden estos elementos al panel
     * de b�squeda y se llama al m�todo "getPanelBusquedaTreeTable" de la clase
     * padre para formar din�micamente el panel que se regresa.
     *
     * @author   enunez
     * @date     12/09/2011
     */
    @Override
    public RichPanelGroupLayout getPanelBusquedaTreeTable() {
        RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
            this.setTituloPanelBusquedaTreeTable(properties.getMessage("TIT.SECCION_BUSQUEDA"));
            this.setTipPanelBusquedaTreeTable(properties.getMessage("TIP.BUSQUEDA_TREE_TABLE"));
            this.setBuscarPorTreeTable(properties.getMessage("LOV.BUSCAR_POR_TREE_TABLE"));
            this.setCriterioBusquedaTreeTable(properties.getMessage("LOV.CRITERIO_TREE_TABLE"));
            this.setParametroTreeTable(properties.getMessage("NOM.PARAM_TREE_TABLE"));
            this.setBotonBuscarTreeTable(properties.getMessage("BOTON.BUSCAR"));
            this.setColumnas(3);
            List<SelectItem> campos = new ArrayList<SelectItem>();
            /*campos.add(new SelectItem("Campo15", propertiesModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.CAMPO15")));*/
            campos.add(new SelectItem("ClaveSegmento", propertiesModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.CLAVE_SEGMENTO")));
            campos.add(new SelectItem("Descripcion", propertiesModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.DESCRIPCION")));
            /*campos.add(new SelectItem("Nivel", propertiesModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.NIVEL")));
            campos.add(new SelectItem("TipoSegmento", propertiesModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.TIPO_SEGMENTO")));
            campos.add(new SelectItem("Estatus", propertiesModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.ESTATUS")));*/
            //campos.add(new SelectItem("ClaveSegmento", "Clave Segmento"));
            setManagedBean("CataSegCatastralesBean");
            setCamposDeBusquedaTreeTable(campos);
        }
        return super.getPanelBusquedaTreeTable();
    }

    /**
     * Se ejecuta el m�todo de persistencia para filtrar los segmentos
     * catastrales por recaudaci�n base del usuario firmado, mandando llamar
     * primero el m�todo getInfoUser para obtener la recaudaci�n base del
     * usuario y despu�s con el id de la recaudaci�n mandar llamar el m�todo
     * filtrarPorRecaudacion (CataSegCatastralesAMImpl) y filtra todos los
     * segmentos catastrales de acuerdo a la recaudaci�n del usuario.
     */
    @Override
    public void prepareModel(LifecycleContext lifecycleContext) {
        super.prepareModel(lifecycleContext);
        if (!RequestContext.getCurrentInstance().isPostback()) {
            //Obtiene la recaudaci�n base del usuario para filtrar la tabla
            String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
            if ("".equals(idsMunicipios)) {
                muestraPopup("popupRecBase");
            }
            BindingContainer bindings = getBindings();
            OperationBinding ob1 = bindings.getOperationBinding("filtrarMunicipiosXUsuario");
            ob1.getParamsMap().put("muniId", idsMunicipios);
            ob1.execute();
            
            String debug = super.getParametroGeneral("AA0008");
            if (debug == null || debug.equals("")) {
                String msj = properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
            }
        }
    }

    /**
     * M�todo que funciona igual que el insertarRegistro, pero no realiza
     * operaciones de insert del modulo, solo debe habilitar los campos para
     * captura y adem�s mostrar la informaci�n del registro padre en caso de lo
     * tenga, estos campos no son editables. Posteriormente se ejecuta una
     * validaci�n al registro que se desea editar para verificar que pueda ser
     * editado, este m�todo manda llamar al m�todo validaAntesDeEditar
     * (CataSegCatastralesAMImpl) si no regresa error entonces se habilitan los
     * campos para su edici�n, si la validaci�n es correcta entonces se hace
     * commit y se guardan los cambios.
     */
    public String modificaRegistro() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("validaAntesDeEditar");
        String validaEditar = operationBinding.execute().toString();
        String modifica = "true";
        if (!validaEditar.equals("Ok")) {
            this.mostrarMensaje(FacesMessage.SEVERITY_INFO, validaEditar);
            modifica = "false";
        }
        AttributeBinding op1 = (AttributeBinding)getBindings().getControlBinding("selectedRow");
        op1.setInputValue(modifica);

        return null;
    }

    /**
     * @return
     */
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    /**
     * @return
     */
    public String setMunicipio() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
        DCBindingContainer bc =
            (DCBindingContainer)elFactory.createValueExpression(elctx, "#{bindings}", DCBindingContainer.class).getValue(elctx);
        DCIteratorBinding it = bc.findIteratorBinding("AaUsuariosMunicipiosVVO1Iterator");
        for (int i = 0; i < it.getViewObject().getEstimatedRowCount(); i++) {
            Row r = it.getRowAtRangeIndex(i);
            String clave = (String)r.getAttribute("ClaveMunicipio");
            String seleccion = municipios.getValue().toString();
            if (seleccion.equals(clave)) {
                oracle.binding.AttributeBinding PcMuniIdentificador =
                    (oracle.binding.AttributeBinding)getBindings().getControlBinding("PcMuniIdentificador");
                PcMuniIdentificador.setInputValue(r.getAttribute("Identificador"));
            }
        }
        return null;
    }

    /**
     * @param cancelarTransaccion
     */
    public void setCancelarTransaccion(RichDialog cancelarTransaccion) {
        this.cancelarTransaccion = cancelarTransaccion;
    }

    /**
     * @return
     */
    public RichDialog getCancelarTransaccion() {
        return cancelarTransaccion;
    }

    /**
     * @return
     */
    public String getNivel() {
        String nivel = this.getParametroGeneral("PR001");
        return nivel;
    }

    /**
     * @param popupFetchEvent
     */
    public void editPopupFetchListener(PopupFetchEvent popupFetchEvent) {
        String clientId = accion;
        Row row = null;
        if (clientId.contains("Insert")) {
            OperationBinding operationBinding = getBindings().getOperationBinding("CreateInsert");
            if (clientId.contains("Root")) {
                this.getSecaPadre().setRendered(false);
                operationBinding.execute();
                this.setMunicipio();
                AttributeBinding ab = (AttributeBinding)getBindings().getControlBinding("Nivel");
                ab.setInputValue(1);
            } else {
                //RowTreeTable rtt = this.getRowTreeTable();
                row = obtenerFilaSeleccionadaTT();
                Integer nivelHijo = Integer.parseInt(row.getAttribute("Nivel").toString()) + 1;
                operationBinding.execute();
                AttributeBinding ab2 = (AttributeBinding)getBindings().getControlBinding("PrSecaIdentificador");
                ab2.setInputValue(Integer.parseInt(row.getAttribute("Identificador").toString()));
                AttributeBinding ab = (AttributeBinding)getBindings().getControlBinding("Nivel");
                ab.setInputValue(Integer.parseInt(nivelHijo.toString()));
                this.getSecaPadre().setRendered(true);
                this.setMunicipio();
            }
        } else if (clientId.contains("Edit")) {
            //RowTreeTable rtt = this.getRowTreeTable();
            row = obtenerFilaSeleccionadaTT();
            filtrarSegCatastrales(row);
            //Se guarda en flujo de memoria la claveSegmento que esta insertada
            if (row != null) {
                String claveSegmentoInsertada = row.getAttribute("ClaveSegmento").toString();
                this.getPageFlowScope().put("claveSegmentoInsertada", claveSegmentoInsertada);
            }
        }
    }

    public String muestraPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").show();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n", null));
        }
        return null;
    }

    private void filtrarSegCatastrales(Row row) {
        OperationBinding op = (OperationBinding)this.getBindings().getControlBinding("filtrarSegCatastrales");
        op.getParamsMap().put("serie", row.getAttribute("Serie").toString());
        op.getParamsMap().put("id", row.getAttribute("Identificador").toString());
        op.execute();
    }

    /**
     * @param popupDml
     */
    public void setPopupDml(RichPopup popupDml) {
        this.popupDml = popupDml;
    }

    /**
     * @return
     */
    public RichPopup getPopupDml() {
        return popupDml;
    }

    public void borrarDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            //RowTreeTable rowTreeTable = this.getRowTreeTable();
            Row row=obtenerFilaSeleccionadaTT();
            if (row == null) {
                mostrarMensaje(null, properties.getMessage("SYS.REGISTRO"));
                return;
            }
            /*Row row = rowTreeTable.getRow();
            Boolean isLeaf = rowTreeTable.isLeaf();
            if (!isLeaf) {
                mostrarMensaje(FacesMessage.SEVERITY_WARN, properties.getMessage("SYS.SEGMENTO_TIENE_HIJOS"));
                return;
            }*/
            JUCtrlHierNodeBinding nodo =obtenerNodoSeleccionadoTT();
            if (nodo.getChildren()!=null) {
                mostrarMensaje(FacesMessage.SEVERITY_WARN, properties.getMessage("SYS.SEGMENTO_TIENE_HIJOS"));
                return;
            }
                        
            OperationBinding ob = (OperationBinding)this.getBindings().getControlBinding("validaAntesDeBorrar");
            ob.getParamsMap().put("segmentoSerie", Integer.parseInt(nodo.getRow().getAttribute("Serie").toString()));
            ob.getParamsMap().put("segmentoId", Long.parseLong(nodo.getRow().getAttribute("Identificador").toString()));
            String respuesta = (String)ob.execute();
            if (respuesta.equals("OK")) {
                AttributeBinding selectedRow = (AttributeBinding)this.getBindings().getControlBinding("selectedRow");
                selectedRow.setInputValue("false");
                nodo.getRow().remove();
                commitOperation();
                refreshTable();
                mostrarMensaje(FacesMessage.SEVERITY_INFO, properties.getMessage("SYS.OPERACION_EXITOSA"));
            } else if (respuesta.equals("ERROR")) {
                mostrarMensaje(null, "Ha ocurrido un error.");
            } else {
                mostrarMensaje(null, respuesta);
            }
        }
    }

    private void commitOperation() {
        OperationBinding operationBinding = getBindings().getOperationBinding("Commit");
        operationBinding.execute();
    }

    private void rollbackOperation() {
        OperationBinding operationBinding = getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
    }

    public void setSecaPadre(RichSelectOneChoice secaPadre) {
        this.secaPadre = secaPadre;
    }

    public RichSelectOneChoice getSecaPadre() {
        return secaPadre;
    }

    /**
     * M�todo para mandar llamar a validaClaveMpio en persistencia con la claveSegmento y el recaId.
     */
    public boolean validaClaveMpio(String claveSegmento, String recaId) {
        OperationBinding ob = (OperationBinding)this.getBindings().getControlBinding("validaClaveMpio");
        ob.getParamsMap().put("claveSegmento", claveSegmento);
        ob.getParamsMap().put("recaId", recaId);
        String respuesta = (String)ob.execute();
        if (respuesta.equals("true")) {
            return true;
        }
        return false;
    }

    /**
     * M�todo que manda llamar al m�todo de persistencia de validarClave. Se le
     * manda la clave catastral completa que se quiere validar si ya existe en
     * base de datos o no y regresa un Boolean para saber si la clave es v�lida
     * o no.
     *
     * @param claveAValidar
     * @return
     *
     * @author Victor Venegas
     * @versi�n 1.0 12-12-2013
     * @component Cat�logo de segmentos catastrales
     */
    public Boolean validarClave(String claveAValidar) {
        OperationBinding ob = (OperationBinding)this.getBindings().getControlBinding("validarClave");
        ob.getParamsMap().put("claveAValidar", claveAValidar);
        Boolean respuesta = (Boolean)ob.execute();
        return respuesta;
    }

    /**
     * M�todo para mandar llamar a validaClave en persistencia con la claveSegmento y el secaId
     */
    public void setNivelSegmento(RichInputText nivelSegmento) {
        this.nivelSegmento = nivelSegmento;
    }

    public RichInputText getNivelSegmento() {
        return nivelSegmento;
    }

    public void realizarValidaciones(ActionEvent actionEvent) {
        String clientId;
        //-----------------------------------------------------------------
        FacesContext facesContext = FacesContext.getCurrentInstance();
        boolean val = false;
        //------------------------------------------------------------------
        clientId = actionEvent.getComponent().getId();
        if (clientId.contains("Insert")) {
            if (!clientId.contains("Root")) {
                //RowTreeTable rtt = this.getRowTreeTable();
                Row row = obtenerFilaSeleccionadaTT();
                if (row == null) {
                    this.mostrarMensaje(FacesMessage.SEVERITY_WARN, properties.getMessage("SYS.REGISTRO"));
                    return;
                }

                Integer nivelHijo = Integer.parseInt(row.getAttribute("Nivel").toString()) + 1;
                /*Integer nivelACrear =
                Integer.parseInt(this.getParametroGeneral("PR002"));*/
                //VALIDA QUE TENGA EL VALOR PR002----------------------------------------------------
                Integer nivelACrear = 0;
                String valPar = getParametroGeneral("PR002");

                if (valPar != null) {
                    val = true;
                    nivelACrear = Integer.parseInt(this.getParametroGeneral("PR002"));
                } else {
                    val = false;
                    facesContext.addMessage(null,
                                            new FacesMessage(properties.getMessage("PARAMETRO_PR002_NO_CONFIGURADO")));
                }
                //----------------------------------------------------------------------------------
                if (nivelHijo > nivelACrear) {
                    this.mostrarMensaje(FacesMessage.SEVERITY_WARN,
                                        properties.getMessage("SYS.EXCEDE_NIVEL") + " " + nivelACrear);
                    return;
                }

            }
        } else if (clientId.contains("Edit")) {
            //RowTreeTable rtt = this.getRowTreeTable();
            Row row = obtenerFilaSeleccionadaTT();
            if (row == null) {
                this.mostrarMensaje(FacesMessage.SEVERITY_WARN, properties.getMessage("SYS.REGISTRO"));
                return;
            }
            //row = rtt.getRow();
            //Boolean isParent = rtt.isParent();
            JUCtrlHierNodeBinding nodo = obtenerNodoSeleccionadoTT();
            if (nodo.getChildren() != null) {
                mostrarMensaje(FacesMessage.SEVERITY_WARN, properties.getMessage("SYS.SEGMENTO_TIENE_HIJOS_MOD"));
                return;
            }
            OperationBinding ob = (OperationBinding)this.getBindings().getControlBinding("validaAntesDeEditar");
            ob.getParamsMap().put("segmentoSerie", Integer.parseInt(row.getAttribute("Serie").toString()));
            ob.getParamsMap().put("segmentoId", Long.parseLong(row.getAttribute("Identificador").toString()));
            String respuesta = (String)ob.execute();
            if (respuesta.equals("OK")) {
                val = true;
                //Validaciones exitosas
            } else if (respuesta.equals("ERROR")) {
                val = false;
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, properties.getMessage("SYS.OPERACION_FALLIDA"));
                return;
            } else {
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, respuesta);
                return;
            }
        }
        accion = clientId;
        //--------------------------------------------------------------------------------
        if (val || clientId.contains("Root")) {
            muestraPopup();
        }
        //--------------------------------------------------------------------------------

        //muestraPopup();
    }

    //Evento original cancelar del popup cancelar
    /*public void cancelarDialogListener(DialogEvent dialogEvent) {
    rollbackOperation();
    ocultaPopup();
  }
  */

    /**
     * Fecha:28/04/2015
     * Origen:MRXVII-95(RE - Catastro - MR0017 - DGO - Cat�logo de segmentos catastrales - No permite cancelar el alta muestra que faltan capturar campos.)
     * Autor:Alejandro Becerril.
     * */
    public void cancelarDialogListener(ActionEvent actionEvent) {
        popupCancelar.hide();
    }

    /**
     * Fecha:28/04/2015
     * Origen:MRXVII-95(RE - Catastro - MR0017 - DGO - Cat�logo de segmentos catastrales - No permite cancelar el alta muestra que faltan capturar campos.)
     * Autor:Alejandro Becerril.
     * */
    public void AceptarDialogListener(ActionEvent actionEvent) {
        popupCancelar.hide();
        rollbackOperation();
        ocultaPopup();
    }
    public String ocultaPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n", null));
        }
        return null;
    }

    public String muestraPopupCancel() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupCancelar.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").show();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n", null));
        }
        return null;
    }

    public void setPopupCancelar(RichPopup popupCancelar) {
        this.popupCancelar = popupCancelar;
    }

    public RichPopup getPopupCancelar() {
        return popupCancelar;
    }

    /**
     * M�todo que se manda llamar al momento de dar clic en el bot�n de "Aceptar"
     * en el pop up de alta o modificaci�n del cat�logo. Verifica si la acci�n
     * que se realiz� es de alta o modificaci�n para mandar llamar al m�todo de
     * persistencia de armarClave que va a recibir dicha acci�n junto con el
     * identificador del segmento y la clave del segmento para armar la clave
     * catastral completa y despu�s validarla con el m�todo de validarClave.
     * Al final para realizar el commit se manda la clave catastral completa al
     * campo15 para ser guardada en base de datos.
     *
     * @param actionEvent
     * @return
     *
     * @author Victor Venegas
     * @versi�n 1.0 12-12-2013
     * @component Cat�logo de segmentos catastrales
     */
    public void aceptarListener(ActionEvent actionEvent) {
        String nivelInsertado = "";
        String secaId = "";
        String nivelMunicipio = getNivel();
        String claveSegmento = this.getBindings().getControlBinding("ClaveSegmento").toString();
        String recaId = super.getInfoUser().get(PublicKeys.LOGIN_RECAUDACION_ID).toString();
        String action = "";
        //condici�n para determinar el nivel actual.
        if (accion.contains("Root")) {
            //Si insertas un root, ya sabes que el secaId es null.
            nivelInsertado = "1";
            secaId = "";
            action = "insert";
        } else { //El resto de los botones. Ya no aplica el "Root"
            if (accion.contains("Insert")) {
                //Si insertas un hijo, tienes que obtener el secaId.
                //RowTreeTable rtt = this.getRowTreeTable();
                Row row = obtenerFilaSeleccionadaTT();
                Integer nivelTreeTable = Integer.parseInt(row.getAttribute("Nivel").toString()) + 1;
                nivelInsertado = nivelTreeTable.toString();
                secaId = row.getAttribute("Identificador").toString();
                action = "insert";
            } else if (accion.contains("Edit")) {
                //Si insertas un hijo, tienes que obtener el secaId.
                //RowTreeTable rtt = this.getRowTreeTable();
                Row row = obtenerFilaSeleccionadaTT();
                Integer nivelTreeTable = Integer.parseInt(row.getAttribute("Nivel").toString());
                nivelInsertado = nivelTreeTable.toString();
                secaId = row.getAttribute("Identificador").toString();
                action = "update";
            }
        }
        //Validaci�n para el municipio. Solo aplica si el nivel insertado es el mismo que el nivel municipio.
        if (nivelMunicipio != null && nivelMunicipio.equals(nivelInsertado)) {
            muestraPopup();
            if (!validaClaveMpio(claveSegmento, recaId)) {
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, properties.getMessage("SYS.CLAVE_MUNICIPIO_INVALIDA"));
                //Anterior "Asignaci�n de clave de Municipio inv�lida. Municipio no corresponde con la recaudaci�n."
                return;
            }
        }

        OperationBinding ob = (OperationBinding)this.getBindings().getControlBinding("armarClave");
        ob.getParamsMap().put("action", action);
        ob.getParamsMap().put("claveSegmento", claveSegmento);
        ob.getParamsMap().put("secaId", secaId);
        String claveArmada = (String)ob.execute();

        //Validaci�n para la clave �nica. Est� preparado para recibir el secaId nulo.
        Boolean claveValida = validarClave(claveArmada);
        //Si clave es valida quiere decir que la clave ya existe
        if (!claveValida) {
            if (action.equals("update")) {
                //Si la accion es actualizar entonces se debe validar que la clave puede ser igual
                String claveSegmentoInsertada = this.getPageFlowScope().get("claveSegmentoInsertada").toString();
                //Si es una actualizacion y se quiere usar otra clave que ya exista
                if (!claveSegmentoInsertada.equals(claveSegmento)) {
                    muestraPopup();
                    mostrarMensaje(FacesMessage.SEVERITY_ERROR,
                                   properties.getMessage("SYS.CLAVE_CATASTRAL_DUPLICADA"));
                    return;
                }
            } else {
                //Si la clave ya existe y la accion es diferente a actualizar
                muestraPopup();
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, properties.getMessage("SYS.CLAVE_CATASTRAL_DUPLICADA"));
                return;
            }
        }

        AttributeBinding ab = (AttributeBinding)getBindings().getControlBinding("Campo15");
        ab.setInputValue(claveArmada);

        commitOperation();
        mostrarMensaje(FacesMessage.SEVERITY_INFO, properties.getMessage("SYS.OPERACION_EXITOSA"));
        ocultaPopup();
        //Anterior: "Inserci�n de registro exitosa.");
        refreshTreeIterator("PrSegmentosCatastralesVO1Iterator");
    }

    /**
     * Fecha:12/05/2015
     * Origen:RE - Catastro - MR0017_1 - DGO - Cat�logo de segmentos catastrales - Se desaparecen los botones al querer agregar un hijo de tercer nivel.
     * Autor:Brenda Gomez.
     * */
    public Row obtenerFilaSeleccionadaTT() {
        Row row = null;
        JUCtrlHierNodeBinding node = obtenerNodoSeleccionadoTT();
        if (node != null) {
            row = node.getRow();
        }
        return row;
    }

    /**
     * Fecha:12/05/2015
     * Origen:RE - Catastro - MR0017_1 - DGO - Cat�logo de segmentos catastrales - Se desaparecen los botones al querer agregar un hijo de tercer nivel.
     * Autor:Brenda Gomez.
     * */
    public JUCtrlHierNodeBinding obtenerNodoSeleccionadoTT() {
        RowKeySet rks = getTreeTable().getSelectedRowKeys();
        JUCtrlHierNodeBinding nodo = null;
        if (rks != null) {
            CollectionModel treeModel = (CollectionModel)this.getTreeTable().getValue();
            JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)treeModel.getWrappedData();
            List firstSet = (List)rks.iterator().next();
            nodo = treeBinding.findNodeByKeyPath(firstSet);
        }
        return nodo;
    }

    public void setMunicipios(RichSelectOneChoice municipios) {
        this.municipios = municipios;
    }

    public RichSelectOneChoice getMunicipios() {
        return municipios;
    }
}



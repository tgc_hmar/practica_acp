package mx.tgc.view.backing.catalogo;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.RowTreeTable;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class CataBloquesPrediosBean extends GenericBean {
    private RichPopup popupForma;
    private RichSelectOneChoice bloquePadre;
    private PropertiesReader propertiesReaderView;
    private PropertiesReader propertiesReaderModel;
  private RichDialog popupCancelar;
  

  public CataBloquesPrediosBean() {
      if (propertiesReaderView == null) {
          try {
              propertiesReaderView =
                      new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                           CataSegCatasUbicaBean.class);
          } catch (IOException ioe) {
              ioe.printStackTrace();
          }
      }
      if (propertiesReaderModel == null) {
          try {
              propertiesReaderModel =
                      new PropertiesReader("/mx/tgc/model/ModelBundle.properties",
                                           CataSegCatasUbicaBean.class);
          } catch (IOException ioe) {
              ioe.printStackTrace();
          }
      }
    }

    /**
     * Getters y Setters
     */
    public void setPopupForma(RichPopup popupForma) {
        this.popupForma = popupForma;
    }

    public RichPopup getPopupForma() {
        return popupForma;
    }

    public void setBloquePadre(RichSelectOneChoice bloquePadre) {
        this.bloquePadre = bloquePadre;
    }

    public RichSelectOneChoice getBloquePadre() {
        return bloquePadre;
    }

    /**
     * Este m�todo determina el nombre de los atributos por los cuales se van
     * a poder realizar las b�squedas. Estos atributos son "Nombre" y "Etiqueta"
     * y se guardan en una lista de objetos SelectItem. Tambi�n contiene la
     * especificaci�n del Managed Bean que corresponde a este cat�logo.
     * Finalmente se a�aden estos elementos al panel de b�squeda y se llama al
     * m�todo "getPanelBusquedaTreeTable" de la clase padre para formar din�micamente
     * el panel que se regresa.
     *
     * @author   enunez
     * @date     12/09/2011
     */
    @Override
    public RichPanelGroupLayout getPanelBusquedaTreeTable() {
        RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
          this.setTituloPanelBusquedaTreeTable(propertiesReaderView.getMessage("TIT.SECCION_BUSQUEDA") +
                                               " " +
                                               propertiesReaderView.getMessage("NOM.BLOQUES"));
          this.setTipPanelBusquedaTreeTable(propertiesReaderView.getMessage("TIP.BUSQUEDA_TREE_TABLE"));
          this.setBuscarPorTreeTable(propertiesReaderView.getMessage("LOV.BUSCAR_POR_TREE_TABLE"));
          this.setCriterioBusquedaTreeTable(propertiesReaderView.getMessage("LOV.CRITERIO_TREE_TABLE"));
          this.setParametroTreeTable(propertiesReaderView.getMessage("NOM.PARAM_TREE_TABLE"));
          this.setBotonBuscarTreeTable(propertiesReaderView.getMessage("BOTON.BUSCAR"));
            this.setColumnas(3);
            List<SelectItem> campos = new ArrayList<SelectItem>();
            campos.add(new SelectItem("Nombre", propertiesReaderModel.getMessage("PR_PRED_BLOQUES_AGRUPACIONES.LABEL.NOMBRE")));
            campos.add(new SelectItem("Etiqueta", propertiesReaderModel.getMessage("PR_PRED_BLOQUES_AGRUPACIONES.LABEL.ETIQUETA")));
            setManagedBean("CataBloquesPrediosBean");
            setCamposDeBusquedaTreeTable(campos);
        }
        return super.getPanelBusquedaTreeTable();
    }

    /**
     * M�todos ligados a los botones del tree table.
     */

    /**
     * Este m�todo se ejecuta cuando se presiona uno de los botones dentro del
     * popup de borrar fila. Si se presiona "ok" se ejecuta un m�todo que valida
     * si la tabla tiene hijos e impedir el borrado. Tambi�n valida que se haya
     * seleccionado una fila. Despu�s de las validaciones se remueve la fila
     * seleccionada se hace commit y se manda refrescar la ventana actual para
     * proyectar los cambios.
     *
     * @param    dialogEvent
     * @author   enunez
     * @date     09/09/2011
     */
    public void borrarDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            RowTreeTable rowTreeTable = getRowTreeTable();
            if (rowTreeTable == null) {
                mostrarMensaje(FacesMessage.SEVERITY_WARN,
                              propertiesReaderView.getMessage("SYS.REGISTRO"));
            } else {
                Row row = rowTreeTable.getRow();
                oracle.jbo.domain.Number idBloque=
                    (oracle.jbo.domain.Number)row.getAttribute("Identificador");
                Boolean isLeaf = rowTreeTable.isLeaf();
                if (!isLeaf) {
                    mostrarMensaje(FacesMessage.SEVERITY_WARN,
                                    propertiesReaderView.getMessage("SYS.BLOQUE_TIENE_HIJOS"));
                } else if(validaBloqueCaracteristica(idBloque)) {
                    mostrarMensaje(FacesMessage.SEVERITY_WARN,
                                    propertiesReaderView.getMessage("SYS.BLOQUE_TIENE_CARACTERISTICAS"));
                } else{
                    row.remove();
                    refreshTable();
                    commitOperation();
                }
            }
        }
    }

    /**
     * Este m�todo se ejecuta cuando se presiona uno de los botones dentro del
     * popup de la forma, al agregar o modificar fila. Si se presiona "ok" se
     * realiza un commit y se refresca el tree table para proyectar los cambios.
     * De otra forma se hace rollback.
     *
     * @param    dialogEvent
     * @author   enunez
     * @date     09/09/2011
     */
    public void formaDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            commitOperation();
            refreshTreeIterator("PrPredBloquesAgrupacionesVO1Iterator");
        } else if (dialogEvent.getOutcome().name().equals("cancel")) {
            rollbackOperation();
        }
    }

    /**
     *  M�todo que se ejecuta cuando el dialogo de la forma de captura es cancelado.
     *  Se realiza un rollback de la operaci�n.
     *
     * @param    popupCanceledEvent
     * @author   enunez
     * @date     09/09/2011
     */
    public void formaPopupCanceledListener(PopupCanceledEvent popupCanceledEvent) {
        rollbackOperation();
    }

    /**
     * M�todo que se ejecuta al ser llamado el popup con la forma de captura.
     * Contiene la l�gica para diferenciar si se va a agregar un nodo ra�z, un
     * nodo hijo o modificar un nodo existente. En caso de que se modifique un
     * nodo existente se manda llamar el m�todo filtrarBloquesPredios para cargar
     * los datos en la forma de captura.
     *
     * @param    popupFetchEvent
     * @author   enunez
     * @date     09/09/2011
     */
    public void formaPopupFetchListener(PopupFetchEvent popupFetchEvent) {
        //Primero se detecta qu� bot�n mand� llamar al popup.
        String clientId = popupFetchEvent.getLaunchSourceClientId();
        //Si fue uno de los botones de insertar nuevo
        if (clientId.contains("Insert")) {
            OperationBinding operationBinding =
                getBindings().getOperationBinding("CreateInsert");
            //Se revisa que sea nodo ra�z
            if (clientId.contains("Root")) {
                //Si es ra�z no se muestra el selectOneChoice con el identificador del padre.
                this.getBloquePadre().setRendered(false);
                operationBinding.execute();
            } else {
                //Se valida que se haya seleccionado una fila del tree table
                RowKeySet rks = getTreeTable().getSelectedRowKeys();
                if (rks == null) {
                    this.getPopupForma().cancel();
                    mostrarMensaje(FacesMessage.SEVERITY_WARN, propertiesReaderView.getMessage("SYS.REGISTRO"));
                } else {
                    CollectionModel treeModel = (CollectionModel) this.getTreeTable().getValue();
                    JUCtrlHierBinding treeBinding =
                     (JUCtrlHierBinding) treeModel.getWrappedData();
                    List firstSet = (List)rks.iterator().next();
                    JUCtrlHierNodeBinding node =
                     treeBinding.findNodeByKeyPath(firstSet);
                    Row row = node.getRow();
                    //Si es nodo hijo se muestra el selectOneChoice con el identificador del padre.
                    operationBinding.execute();
                    AttributeBinding ab =
                        (AttributeBinding)getBindings().getControlBinding("PrPrbaIdentificador1");
                    ab.setInputValue(row.getAttribute("Identificador"));
                    this.getBloquePadre().setRendered(true);
                }
            }
            //Si el bot�n de modificar lanz� el popup
        } else if (popupFetchEvent.getLaunchSourceClientId().contains("Edit")) {
            //Valida que se haya seleccionado una fila del tree table
            Row row = getSelectedRowTreeTable();
            if (row == null) {
                this.getPopupForma().cancel();
                mostrarMensaje(FacesMessage.SEVERITY_WARN, propertiesReaderView.getMessage("SYS.REGISTRO"));
            } else {
                //Se obtiene el bloque que se va a modificar
                filtrarBloquesPredios(row);
                this.getBloquePadre().setRendered(true);
                if (this.getBloquePadre().getValue().equals(0)) {
                    this.getBloquePadre().setRendered(false);
                }
            }
        }
    }

    /**
     * M�todos auxiliares
     */

    /**
     * M�todo que manda llamar el m�todo filtrarBloquesPredios en persistencia para
     * mostrar en la forma de captura el bloque que se desea modificar.
     *
     * @param    row        La fila que se va a modificar.
     * @author   enunez
     * @date     09/09/2011
     */
    private void filtrarBloquesPredios(Row row) {
        OperationBinding op =
            (OperationBinding)this.getBindings().getControlBinding("filtrarBloquesPredios");
        op.getParamsMap().put("idBloque",
                              row.getAttribute("Identificador").toString());
        op.execute();
    }

    /**
   * M�todo reutilizable para hacer Commit a la operaci�n.
   *
   * @author   enunez
   * @date     09/09/2011
   */
  private void commitOperation() {
    OperationBinding ob = getBindings().getOperationBinding("Commit");
    ob.execute();
    ob.getErrors();
    if (!ob.getErrors().isEmpty()) {
      mostrarMensaje(FacesMessage.SEVERITY_ERROR,
                     propertiesReaderView.getMessage("SYS.OPERACION_FALLIDA"));
      rollbackOperation();
    } else {
      mostrarMensaje(FacesMessage.SEVERITY_INFO,
                     propertiesReaderView.getMessage("SYS.OPERACION_EXITOSA"));
    }
  }

    /**
     * M�todo reutilizable para hacer Rollback a la operaci�n.
     *
     * @author   enunez
     * @date     09/09/2011
     */
    private void rollbackOperation() {
        OperationBinding operationBinding =
            getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
    }

    /**
     * M�todo generado para obtener los bindings del cat�logo y apoyar
     * a su obtenci�n en otros m�todos.
     */
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
   
   public String ocultaPopup() {
       try {
           ExtendedRenderKitService erks =
               Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                           ExtendedRenderKitService.class);
           StringBuilder strb =
               new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                 this.popupForma.getClientId(FacesContext.getCurrentInstance()) +
                                 "\").hide();");
           erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
       } catch (Exception e) {
           FacesContext context = FacesContext.getCurrentInstance();
           context.addMessage(null,
                              new FacesMessage(FacesMessage.SEVERITY_INFO,
                                               "Error en operaci�n", null));
       }
       return null;
   }
  public void aceptarListener(ActionEvent actionEvent) {
        commitOperation();
        refreshTreeIterator("PrPredBloquesAgrupacionesVO1Iterator");
        ocultaPopup();
    }
  
  public String muestraPopup() {
      try {
          ExtendedRenderKitService erks =
              Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                          ExtendedRenderKitService.class);
          StringBuilder strb =
              new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                this.popupForma.getClientId(FacesContext.getCurrentInstance()) +
                                "\").show();");
          erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
      } catch (Exception e) {
          FacesContext context = FacesContext.getCurrentInstance();
          context.addMessage(null,
                             new FacesMessage(FacesMessage.SEVERITY_INFO,
                                              "Error en operaci�n", null));
      }
      return null;
  }
  
  public void cancelarListener(ActionEvent actionEvent) {
    rollbackOperation();
    resetComponent(popupForma);
  }

  public void setPopupCancelar(RichDialog popupCancelar) {
    this.popupCancelar = popupCancelar;
  }

  public RichDialog getPopupCancelar() {
    return popupCancelar;
  }
  
  /**
  * M�todo que valida si un bloqueCaracteristicas tiene caracteristicas asignadas
  *
  * @param    idBloque   Identificador del bloqueCaracteristicas
  * @return   res        Respuesta del metodo, true si el bloqueCaracteristicas tiene
  * caracteristicas asignadas, false en caso contrario.
  * @author   Juan Carlos Olivas
  * @date     30/04/2014
  */  
  private Boolean validaBloqueCaracteristica(oracle.jbo.domain.Number idBloque){
      Boolean res=false;
      OperationBinding op =
       (OperationBinding)this.getBindings().getControlBinding("validaBloqueCaracteristica");
      op.getParamsMap().put("idBloque",idBloque);
      res=(Boolean)op.execute();
      return res;
  }

    /*
    public void setTreeTable(RichTreeTable treeTable) {
        this.treeTable = treeTable;
    }

    public RichTreeTable getTreeTable() {
        return treeTable;
    }
    */
  
    public Row getSelectedRowTreeTable() {
        RowKeySet rks = getTreeTable().getSelectedRowKeys();
        Row row = null;
        if (rks != null) {
            CollectionModel treeModel = (CollectionModel)this.getTreeTable().getValue();
            JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)treeModel.getWrappedData();
            List firstSet = (List)rks.iterator().next();
            JUCtrlHierNodeBinding node = treeBinding.findNodeByKeyPath(firstSet);
            row = node.getRow();
        }
        return row;
    }
}

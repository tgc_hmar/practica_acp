package mx.tgc.view.backing.catalogo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;
import mx.tgc.utilityfwk.view.search.SearchComponent;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.output.RichMessage;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.JboException;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class CataCaractAsentamientosBean extends GenericBean {
  private RichPopup formaPopUp;
  private RichInputText nombreCaracteristica;
  private RichTable t2;
  private RichPopup catalogoPopUp;
  private RichMessage errorCaracteristica;
  private RichTable t1;
  private RichInputText nomAsentamientoPadre;
  PropertiesReader properties;

  /**
   * This is the default constructor (do not remove).
   */
  public CataCaractAsentamientosBean() {
    if (properties == null) {
      try {
        properties =
            new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                 CataCaractAsentamientosBean.class);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public void setNombreCaracteristica(RichInputText nombre) {
    this.nombreCaracteristica = nombre;
  }

  public RichInputText getNombreCaracteristica() {
    return nombreCaracteristica;
  }

  public void setT2(RichTable t2) {
    this.t2 = t2;
  }

  public RichTable getT2() {
    return t2;
  }

  public void setFormaPopUp(RichPopup formaPopUp) {
    this.formaPopUp = formaPopUp;
  }

  public RichPopup getFormaPopUp() {
    return formaPopUp;
  }

  public void setCatalogoPopUp(RichPopup catalogoPopUp) {
    this.catalogoPopUp = catalogoPopUp;
  }

  public RichPopup getCatalogoPopUp() {
    return catalogoPopUp;
  }

  public void setErrorCaracteristica(RichMessage errorCaracteristica) {
    this.errorCaracteristica = errorCaracteristica;
  }

  public RichMessage getErrorCaracteristica() {
    return errorCaracteristica;
  }

  public void setT1(RichTable t1) {
    this.t1 = t1;
  }

  public RichTable getT1() {
    return t1;
  }

  public void setNomAsentamientoPadre(RichInputText nomAsentamientoPadre) {
    this.nomAsentamientoPadre = nomAsentamientoPadre;
  }

  public RichInputText getNomAsentamientoPadre() {
    return nomAsentamientoPadre;
  }

  /**
   * B�squeda Custom
   * @return
   */
  public List<SearchComponent> prepararBusqueda() {
    List<SearchComponent> l = new ArrayList<SearchComponent>();
    //Consulta de par�metro PR0009 que contendr� el valor que llevar� la
    //etiqueta configurable (Colonia o asentamiento).
    String etiqueta = (String)getPageFlowScope().get("PR0009");

    SearchComponent sc1 = new SearchComponent("cataCaractAsentamientosBean");
    sc1.setComponente(SearchComponent.INPUT_TEXT);
    sc1.setAtributo("ClaveAsentamiento");
    if (etiqueta.equals("ASENTAMIENTO")) {
      sc1.setLabel("Clave asentamiento:");
    } else if (etiqueta.equals("COLONIA")) {
      sc1.setLabel("Clave colonia:");
    }
    sc1.setSize(6);
    sc1.setId("txtClaveAsenatmiento");

    SearchComponent sc2 = new SearchComponent("cataCaractAsentamientosBean");
    sc2.setComponente(SearchComponent.SELECT_ONE_CHOICE);
    sc2.setAtributo("TipoAsentamiento");
    if (etiqueta.equals("ASENTAMIENTO")) {
      sc2.setLabel("Tipo de asentamiento:");
    } else if (etiqueta.equals("COLONIA")) {
      sc2.setLabel("Tipo de colonia:");
    }
    sc2.setId("socTipoAsentamiento");
    sc2.setDominio("PC_ASENTAMIENTOS.TIPO_ASENTAMIENTO");

    SearchComponent sc3 = new SearchComponent("cataCaractAsentamientosBean");
    sc3.setComponente(SearchComponent.INPUT_TEXT);
    sc3.setAtributo("NombreAsentamiento");
    if (etiqueta.equals("ASENTAMIENTO")) {
      sc3.setLabel("Nombre de asentamiento:");
    } else if (etiqueta.equals("COLONIA")) {
      sc3.setLabel("Nombre de colonia:");
    }
    sc3.setId("txtNombreAsentamiento");

    SearchComponent sc4 = new SearchComponent("cataCaractAsentamientosBean");
    sc4.setComponente(SearchComponent.SELECT_ONE_CHOICE);
    sc4.setAtributo("Estatus");
    sc4.setLabel("Estatus:");
    sc4.setId("socEstatus");
    sc4.setDominio("PC_ASENTAMIENTOS.ESTATUS");
    l.add(sc1);
    l.add(sc2);
    l.add(sc3);
    l.add(sc4);
    setColumnas(1);
    return l;
  }

  /**
   * Es el m�todo del bot�n �Aceptar� que confirma la acci�n de borrar,
   * posteriormente se hace commit.
   * @return
   */
  public String borrarRegistro() {
    BindingContainer bindings = getBindings();
    OperationBinding operationBinding = bindings.getOperationBinding("Delete");
    operationBinding.execute();
    if (!operationBinding.getErrors().isEmpty()) {
      operationBinding = bindings.getOperationBinding("Rollback");
      operationBinding.execute();
      FacesContext fc = FacesContext.getCurrentInstance();
      fc.addMessage(null,
                    new FacesMessage(properties.getMessage("SYS.OPERACION_FALLIDA")));
      return null;
    }
    operationBinding = bindings.getOperationBinding("Commit");
    operationBinding.execute();
    FacesContext fc = FacesContext.getCurrentInstance();
    fc.addMessage(null,
                  new FacesMessage(properties.getMessage("SYS.OPERACION_EXITOSA")));
    return null;
  }

  /**
   * Se ejecuta el m�todo de persistencia para filtrar los segmentos
   * catastrales por recaudaci�n base del usuario firmado, mandando llamar
   * primero el m�todo super.getInfoUser para obtener la recaudaci�n base del
   * usuario y despu�s con el id de la recaudaci�n mandar llamar el m�todo
   * filtrarPorRecaudacion (CataCaracAsentamientoAMImpl) y filtra todos los
   * segmentos catastrales deacuerdo a la recaudaci�n del usuario. Tambi�n se
   * consulta el par�metro PR003 para determinar si las etiquetas en la
   * pantalla deben mostr�r asentamiento(s) o colonia(s).
   * @param lifeCycleContext
   */
  @Override
  public void prepareModel(LifecycleContext lifeCycleContext) {
    super.prepareModel(lifeCycleContext);
    if (!RequestContext.getCurrentInstance().isPostback()) {
        String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
        if ("".equals(idsMunicipios)) {
            muestraPopup("popupRecBase");
        }
        BindingContainer bindings = getBindings();
        OperationBinding ob =
            bindings.getOperationBinding("filtrarMunicipiosXUsuario");
        ob.getParamsMap().put("muniId", idsMunicipios);
        ob.execute();

      //Consulta de par�metro PR0009 que contendr� el valor que llevar� la
      //etiqueta configurable (Colonia o asentamiento).
      String etiqueta = super.getParametroGeneral("PR0009");
      if (etiqueta != null && !etiqueta.equals("")) {
        if (etiqueta.equals("ASENTAMIENTO") || etiqueta.equals("COLONIA")) {
          super.getPageFlowScope().put("PR0009", etiqueta);
        } else {
          super.getPageFlowScope().put("PR0009", "ASENTAMIENTO");
          String msj = properties.getMessage("SYS.PARAMETRO_PR0009_NO_EXISTE");
          mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
        }
      } else {
        super.getPageFlowScope().put("PR0009", "ASENTAMIENTO");
        String msj = properties.getMessage("SYS.PARAMETRO_PR0009_NO_EXISTE");
        mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
      }

        String debug = super.getParametroGeneral("AA0008");
        if(debug == null && debug.equals("")){
            String msj = properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);          
        }
    }
  }

  /**
   * @param popupFetchEvent
   */
  public void formaPopupFetchListener(PopupFetchEvent popupFetchEvent) {
    String clientId = popupFetchEvent.getLaunchSourceClientId();
    AttributeBinding lupa =
      (AttributeBinding)this.getBindings().getControlBinding("MostrarLupa");
    if (clientId.contains("Insert")) {
      //Mostramos a lupita
      lupa.setInputValue(true);
      OperationBinding ob = getBindings().getOperationBinding("CreateInsert");
      ob.execute();
      //Agarrar el asentamiento de la tabla  PCAsentamientosVO2 y ponerselo a NomAsentamientoPadre
      /*JUCtrlHierNodeBinding rowData =
        (JUCtrlHierNodeBinding)t1.getSelectedRowData();
      this.getNomAsentamientoPadre().setValue(rowData.getAttribute("NombreAsentamiento"));
      oracle.adf.model.AttributeBinding ab =
        (oracle.adf.model.AttributeBinding)getBindings().getControlBinding("PrPrbaIdentificador");*/
    } else if (clientId.contains("Edit")) {
      //Escondemos a lupita
      lupa.setInputValue(false);
      //Agarrar el asentamiento de la tabla  PCAsentamientosVO2 y ponerselo a NomAsentamientoPadre
      /*JUCtrlHierNodeBinding rowData =
        (JUCtrlHierNodeBinding)t1.getSelectedRowData();
      this.getNomAsentamientoPadre().setValue(rowData.getAttribute("NombreAsentamiento"));
      //Traer el nombre de la caracteristica
      oracle.adf.model.AttributeBinding ab =
        (oracle.adf.model.AttributeBinding)getBindings().getControlBinding("PrPrbaIdentificador");
      AttributeBinding carac =
        (AttributeBinding)this.getBindings().getControlBinding("Nombre");*/
    }
  }

  /**
   * @param popupCanceledEvent
   */
  public void formaPopupCancelListener(PopupCanceledEvent popupCanceledEvent) {
    rollbackOperation();
    DCBindingContainer bc =
      (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
    DCIteratorBinding ib =
      bc.findIteratorBinding("PrCaractAsentamientosVO2Iterator");
    ib.getViewObject().clearCache();
    ib.getViewObject().executeQuery();
    AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
    adfFacesContext.addPartialTarget(t2);
    adfFacesContext.addPartialTarget(t1);
  }

  /**
   * @param popupFetchEvent
   */
  public void catalogoPopupFetchListener(PopupFetchEvent popupFetchEvent) {
    try {
      //Filtrar los tipos de aval�os que correspondan a la recaudaci�n base
      String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
      OperationBinding ob =
        this.getBindings().getOperationBinding("filtrarTiposAvaluos");
      ob.getParamsMap().put("muniId", idsMunicipios);
      ob.execute();
      //Filtrar la tabla de caracter�sticas asentamientos
      AttributeBinding asen =
        (AttributeBinding)this.getBindings().getControlBinding("asenId");
      //Luego se manda el filtrado de la tabla de predial caracter�sticas
      OperationBinding ob2 =
        this.getBindings().getOperationBinding("filtrarCaractAsentamientos");
      ob2.getParamsMap().put("asenId", asen.getInputValue().toString());
      ob2.execute();
    } catch (Exception e) {
      throw new JboException(e.getMessage());
    }
  }

  /**
   * @param popupCanceledEvent
   */
  public void catalogoPopupCancelListener(PopupCanceledEvent popupCanceledEvent) {
    DCBindingContainer bc =
      (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
    DCIteratorBinding ib =
      bc.findIteratorBinding("PrCaractAsentamientosVO2Iterator");
    ib.getViewObject().clearCache();
    ib.getViewObject().executeQuery();
  }

  /**
   * @param valueChangeEvent
   */
  public void tiposAvaluosChangeListener(ValueChangeEvent valueChangeEvent) {
    try {
      //Aqu� estoy agarrando el asentamiento actual de la caracter�stica
      AttributeBinding asen =
        (AttributeBinding)this.getBindings().getControlBinding("asenId");
      //Luego se manda el filtrado de la tabla de predial caracter�sticas
      BindingContainer bindings = this.getBindings();
      OperationBinding ob =
        bindings.getOperationBinding("filtrarCaractAsentamientos");
      ob.getParamsMap().put("asenId", asen.getInputValue().toString());
      ob.execute();
    } catch (Exception e) {
      throw new JboException(e.getMessage());
    }
  }

  private void commitOperation() {
    OperationBinding ob = getBindings().getOperationBinding("Commit");
    ob.execute();
    if (!ob.getErrors().isEmpty()) {
      mostrarMensaje(FacesMessage.SEVERITY_ERROR,
                     properties.getMessage("SYS.CARACTERISTICA"));
      rollbackOperation();
    } else {
      mostrarMensaje(FacesMessage.SEVERITY_INFO,
                     properties.getMessage("SYS.OPERACION_EXITOSA"));
    }
  }

  private void rollbackOperation() {
    OperationBinding ob = getBindings().getOperationBinding("Rollback");
    ob.execute();
  }

  public void aceptarListener(ActionEvent actionEvent) {
    boolean showMjs = false;
    AttributeBinding prid =
      (AttributeBinding)this.getBindings().getControlBinding("PrPrcaIdentificador");
    if (prid.getInputValue() == null) {
      mostrarMensaje(FacesMessage.SEVERITY_ERROR,
                     properties.getMessage("SYS.CARACTERISTICA_FALTANTE"));
      showMjs = true;
    }
    if (showMjs) {
      return;
    }
    commitOperation();
    AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
    adfFacesContext.addPartialTarget(t2);
    ocultaPopup();
  }

  /* public void cancelarDialogListener(DialogEvent dialogEvent) {
    rollbackOperation();
    ocultaPopup();
  }*/

  public void cancelarListener(ActionEvent actionEvent) {
    rollbackOperation();
      formaPopUp.cancel();
    resetComponent(formaPopUp);
  }

  public String ocultaPopup() {
    try {
      ExtendedRenderKitService erks =
        Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                    ExtendedRenderKitService.class);
      StringBuilder strb =
        new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.formaPopUp.getClientId(FacesContext.getCurrentInstance()) +
                          "\").hide();");
      erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    } catch (Exception e) {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null,
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n",
                                          null));
    }
    return null;
  }

    public String lanzarPopUpCaractValuacion() {
        this.getPageFlowScope().put("catalogo", "false");
        this.getPageFlowScope().put("filtrarNivel", "A");
        DCBindingContainer bc = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding ib = bc.findIteratorBinding("AaUsuariosMunicipiosVVO1Iterator");
        Row municipio = ib.getCurrentRow();
        this.getPageFlowScope().put("municipioSeleccionado", municipio.getAttribute("Identificador").toString());
        return "caractValuacion";
    }

  public void caractValuacionDialogListener(ReturnEvent returnEvent) {
    try {
        if (!returnEvent.getReturnParameters().isEmpty()) {
            AttributeBinding pcPrcaSerie =
                (AttributeBinding)getBindings().getControlBinding("PrPrcaSerie");
            pcPrcaSerie.setInputValue(returnEvent.getReturnParameters().get("serie"));
            AttributeBinding pcPrcaIdentificador =
                (AttributeBinding)getBindings().getControlBinding("PrPrcaIdentificador");
            pcPrcaIdentificador.setInputValue(returnEvent.getReturnParameters().get("identificador"));
            AttributeBinding nombreCaract =
                (AttributeBinding)getBindings().getControlBinding("NombreCaract");
            nombreCaract.setInputValue(returnEvent.getReturnParameters().get("nombre"));
          AttributeBinding locaId =
            (AttributeBinding)this.getBindings().getControlBinding("locaId");
          AttributeBinding locaIdentificador =
            (AttributeBinding)this.getBindings().getControlBinding("LocaIdentificador");
          locaIdentificador.setInputValue(locaId);

        } else {
            rollbackOperation();
        }
    } catch (Exception e) {
        mostrarMensaje(FacesMessage.SEVERITY_ERROR,
                       e.getLocalizedMessage());
        rollbackOperation();
    }finally{
        this.getPageFlowScope().remove("municipioSeleccionado");
    }
  }
}

package mx.tgc.view;


import java.io.PrintStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.FacesMessage;
import javax.faces.component.EditableValueHolder;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import mx.net.tgc.recaudador.resource.pc.BloqueAgrupacion;
import mx.net.tgc.recaudador.resource.pr.SegmentoCatastral;

import mx.tgc.model.ro.configuracion.AaListasValoresNivelSegImpl;

import mx.tgc.model.ro.configuracion.AaListasValoresNivelSegRowImpl;

import mx.tgc.model.view.catastro.PrSegmentosCatVOImpl;

import mx.tgc.model.view.catastro.PrSegmentosCatVORowImpl;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.PopupCanceledEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.sqlj.runtime.Oracle;

import org.apache.myfaces.trinidad.component.UIXComponentBase;
import org.apache.myfaces.trinidad.component.UIXEditableValue;
import org.apache.myfaces.trinidad.component.UIXInput;
import org.apache.myfaces.trinidad.component.UIXSelectBoolean;
import org.apache.myfaces.trinidad.component.UIXSelectInput;
import org.apache.myfaces.trinidad.component.UIXSelectOne;
import org.apache.myfaces.trinidad.component.core.data.CoreTree;
import org.apache.myfaces.trinidad.component.core.input.CoreInputText;
import org.apache.myfaces.trinidad.component.core.nav.CoreCommandLink;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.AttributeChangeEvent;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class CataSegmentosCataBean {
    public CoreCommandLink commandLink2;
    public RichPanelGroupLayout formBusqueda2;
    public static Integer nivel;
    private RichSelectOneChoice combobinding;
    public List SegSeleccionados;
    private RichPopup popUpCrear;
    private RichPopup popupModificar;
    private RichPopup popUpEliminar;

    public CataSegmentosCataBean() {
        
    }


    public String test(){
        Map pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        String clave = getCadena();
        
        Number nivelBus =getNivel();
        //String segmentoBus = "-029-030%";
        String segmentoBus = clave+"%";
        
        OperationBinding operationBinding = getBindings().getOperationBinding("getSegmentoCatastralPadre");
        operationBinding.getParamsMap().put("p_nivel", nivelBus);
        operationBinding.getParamsMap().put("p_clave", segmentoBus);
        SegmentoCatastral segmento = (SegmentoCatastral)operationBinding.execute();
        
        if (segmento!= null){
            
            OperationBinding opFiltraSeg = getBindings().getOperationBinding("filtraSegmentosCatPadre");
            opFiltraSeg.getParamsMap().put("serie", segmento.getSerie().intValue());
            opFiltraSeg.getParamsMap().put("identificador", segmento.getIdentificador());
            opFiltraSeg.execute();
            pageFlowScope.put("filtroaplicado", "TRUE");
            pageFlowScope.put("segmentoMaestro", segmentoBus);
            pageFlowScope.put("segmentoPadre", segmento);
        }    

        return null;
    }
    
    public List getSegmentoABuscSeleccionado() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
        DCBindingContainer bc =
            (DCBindingContainer)elFactory.createValueExpression(elctx, "#{bindings}", DCBindingContainer.class).getValue(elctx);
        DCIteratorBinding it = bc.findIteratorBinding("AaListasValoresNivelSeg1Iterator");
        
        AaListasValoresNivelSegRowImpl segmentoPadre = (AaListasValoresNivelSegRowImpl) it.getCurrentRow();
        Row rows[] = it.getAllRowsInRange();
        List ListaSegmentos = new ArrayList(); 
        Integer cantSegmentos = Integer.parseInt(segmentoPadre.getNivel());
        
        
        for (int i = 0; i<cantSegmentos; i++){
            ListaSegmentos.add(rows[i].getAttributeValues()[5]);
        }
        setNivel(ListaSegmentos.size());
        return ListaSegmentos;
    }
    
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    
    public void setCommandLink2(CoreCommandLink commandLink2) {
        this.commandLink2 = commandLink2;
    }

    public CoreCommandLink getCommandLink2() {
        return commandLink2;
    }


    public RichPanelGroupLayout getSegmentosConfigurados() {
      if (formBusqueda2 == null) {
        formBusqueda2 = new RichPanelGroupLayout();
      }
      return formBusqueda2;
    }


    public void dibujaSegmentos() {
        //borramos y dibujamos el panel nuevamente
        RichPanelGroupLayout paneles =
            createDynamicRichPanel(FacesContext.getCurrentInstance(), RequestContext.getCurrentInstance(),
                                    getCommandLink2());
        paneles.setLayout(RichPanelGroupLayout.LAYOUT_VERTICAL);
        paneles.setValign(RichPanelGroupLayout.VALIGN_TOP);
        paneles.setHalign(RichPanelGroupLayout.HALIGN_CENTER);
        formBusqueda2.getChildren().clear();
        formBusqueda2.getChildren().add(paneles);
    }

    public RichPanelGroupLayout createDynamicRichPanel(FacesContext fc, RequestContext afc,
                                                              CoreCommandLink commandLink2) {
        RichPanelGroupLayout panelPpal = new RichPanelGroupLayout();
        panelPpal.setValign(RichPanelGroupLayout.VALIGN_TOP);

        List children = panelPpal.getChildren();
        
        List cantSegmentos2 = getSegSeleccionados();
        for (int i=0;i<cantSegmentos2.size();i++){
            RichInputText cit1 = new RichInputText();
            cit1.setMaximumLength(9);
            cit1.setLabel(cantSegmentos2.get(i) + ":");
            cit1.setId("i_" + i);
            cit1.setParent(panelPpal);
            children.add(cit1);
        }
        return panelPpal;
        
    }


    
    
    public String getCadena(){
        String clave = "";
        String segmento = "";
        Map segementosConf = new HashMap();
        getAllSelectedComponents(formBusqueda2, segementosConf);
                
  
        List cantSegmentos2 = getSegSeleccionados();
        for (int i = 0; i<segementosConf.size(); i++){
            segmento ="";
            segmento = (String)segementosConf.get("i_"+i);
            if(segmento!=null && !"".equals(segmento)){
                clave  +="-"+segmento;
            }
        }
        
        
        return clave;
    }
    
    public static void getAllSelectedComponents(UIXComponentBase component, 
                                                Map seleccionados) {
        // si el componente recibido se puede limpiar, se hace aqui.
        if (component instanceof CoreTree) {
            Map pageFlowScope = 
                RequestContext.getCurrentInstance().getPageFlowScope();
            seleccionados.put("tree", pageFlowScope.get("hijo"));
        }
        if (component instanceof UIXEditableValue) {
            if (component instanceof EditableValueHolder) {
                EditableValueHolder evh = (EditableValueHolder)component;
                if (evh instanceof UIXSelectBoolean) {
                    UIXSelectBoolean radio = (UIXSelectBoolean)evh;
                    if (radio.isSelected()) {
                        seleccionados.put(radio.getAttributes().get("id"), 
                                          radio.getValue());
                    }
                } else if (evh instanceof UIXInput) {
                    UIXInput input = (UIXInput)evh;
                    if (input.getAttributes().get("id") != null && 
                        input.getValue() != null) {
                        seleccionados.put(input.getAttributes().get("id"), 
                                          input.getValue());
                    }
                } else if (evh instanceof UIXSelectOne) {
                    UIXSelectOne input = (UIXSelectOne)evh;
                    if (input.getAttributes().get("id") != null && 
                        input.getValue() != null) {

                        seleccionados.put(input.getAttributes().get("id"), 
                                          input.getValue());
                    }
                } else if (evh instanceof UIXSelectInput) {
                    UIXSelectInput input = (UIXSelectInput)evh;
                    if (input.getAttributes().get("id") != null && 
                        input.getValue() != null) {

                        seleccionados.put(input.getAttributes().get("id"), 
                                          input.getValue());
                    }
                }
            }
            return;
        }
        // si el comopnente ya no tiene hijos el metodo regresa.
        else if (component.getChildCount() == 0 && 
                 component.getFacetCount() == 0) {
            return;
        } else {
            Iterator iterador = component.getFacetsAndChildren();
            while (iterador.hasNext()) {
                Object objec = iterador.next();
                if (objec instanceof UIXComponentBase) {
                    getAllSelectedComponents((UIXComponentBase)objec, 
                                             seleccionados);
                }
            }
            return;
        }
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public static Integer getNivel() {
        return nivel;
    }

    public void setCombobinding(RichSelectOneChoice combobinding) {
        this.combobinding = combobinding;
    }

    public RichSelectOneChoice getCombobinding() {
        return combobinding;
    }

    public void setFormBusqueda2(RichPanelGroupLayout formBusqueda2) {
        this.formBusqueda2 = formBusqueda2;
    }

    public RichPanelGroupLayout getFormBusqueda2() {
        if (formBusqueda2 == null) {
          formBusqueda2 = new RichPanelGroupLayout();
        }
        return formBusqueda2;
    }

    public String cb3_action() {
        setSegSeleccionados(getSegmentoABuscSeleccionado());
        dibujaSegmentos();
        return null;
    }

    public void setSegSeleccionados(List SegSeleccionados) {
        this.SegSeleccionados = SegSeleccionados;
    }

    public List getSegSeleccionados() {
        if(SegSeleccionados== null){
            SegSeleccionados = new ArrayList();
        }
        return SegSeleccionados;
    }
    
    public String lanza_crear() {
        Map pageFlowScope = AdfFacesContext.getCurrentInstance().getPageFlowScope();
        OperationBinding operationBinding = getBindings().getOperationBinding("CreateInsert");
        operationBinding.execute();
        
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
        
        
        SegmentoCatastral segmentoPadre = (SegmentoCatastral)pageFlowScope.get("segmentoPadre");
        
        Integer cantSegmentos = Integer.parseInt(segmentoPadre.getNivel().toString())+1;
        

        ValueExpression ve = elFactory.createValueExpression(elctx, "#{bindings.Nivel1.inputValue}", Object.class);
        ValueExpression seriePadre = elFactory.createValueExpression(elctx, "#{bindings.PrSecaSerie.inputValue}", Object.class);
        ValueExpression idPadre = elFactory.createValueExpression(elctx, "#{bindings.PrSecaIdentificador.inputValue}", Object.class);
        
        
        //TODO:         *Definir el valor municipio del nuevo registro */
        ValueExpression minIdPadre = elFactory.createValueExpression(elctx, "#{bindings.PcMuniIdentificador.inputValue}", Object.class);

        try{
            ve.setValue(elctx, new oracle.jbo.domain.Number(cantSegmentos));
            seriePadre.setValue(elctx, new oracle.jbo.domain.Number(segmentoPadre.getSerie()));
            idPadre.setValue(elctx, new oracle.jbo.domain.Number(segmentoPadre.getIdentificador()));
            minIdPadre.setValue(elctx, new oracle.jbo.domain.Number(47));
            showPopupCrear();
        }catch (Exception e){
            e.printStackTrace();
            cancel_Crear(new PopupCanceledEvent(popUpCrear));
        }
        
        
        return null;
    }
    
    public void showPopupCrear(){
        FacesContext context = FacesContext.getCurrentInstance();
      
        ExtendedRenderKitService erks = Service.getRenderKitService(context, ExtendedRenderKitService.class);
        StringBuilder strb =  new StringBuilder("AdfPage.PAGE.findComponent(\"" + 
                                                this.popUpCrear.getClientId(context) +
                                                "\").show();");
        erks.addScript(context, strb.toString());
        
    }
    
    public void showPopupModificar(){
        FacesContext context = FacesContext.getCurrentInstance();
      
        ExtendedRenderKitService erks = Service.getRenderKitService(context, ExtendedRenderKitService.class);
        StringBuilder strb =  new StringBuilder("AdfPage.PAGE.findComponent(\"" + 
                                                this.popupModificar.getClientId(context) +
                                                "\").show();");
        erks.addScript(context, strb.toString());
        
    }
    
    public void showPopupEliminar(){
        FacesContext context = FacesContext.getCurrentInstance();
      
        ExtendedRenderKitService erks = Service.getRenderKitService(context, ExtendedRenderKitService.class);
        StringBuilder strb =  new StringBuilder("AdfPage.PAGE.findComponent(\"" + 
                                                this.popUpEliminar.getClientId(context) +
                                                "\").show();");
        erks.addScript(context, strb.toString());
        
    }

    public void ocultaPopupModificar() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupModificar.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operación", null));
        }
    }
    
    public void ocultaPopupCrear() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popUpCrear.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operación", null));
        }
    }
    
    public void ocultaPopupEliminar() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popUpEliminar.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operación", null));
        }
    }

    public String lanza_Modificar() {
        showPopupModificar();
        return null;
    }

    public String lanza_Eliminar() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
        DCBindingContainer bc =
            (DCBindingContainer)elFactory.createValueExpression(elctx, "#{bindings}", DCBindingContainer.class).getValue(elctx);
        DCIteratorBinding it = bc.findIteratorBinding("PrSegmentosCatVO1Iterator");
        
        PrSegmentosCatVORowImpl segmentoABorrar = (PrSegmentosCatVORowImpl) it.getCurrentRow();
        
        if (segmentoABorrar.getSerie()!= null && segmentoABorrar.getIdentificador()!= null){
            OperationBinding ob = (OperationBinding)this.getBindings().getControlBinding("validaAntesDeBorrar");
            ob.getParamsMap().put("segmentoSerie", Integer.parseInt(segmentoABorrar.getSerie().toString()));
            ob.getParamsMap().put("segmentoId", Long.parseLong(segmentoABorrar.getIdentificador().toString()));
            String respuesta = (String)ob.execute();
            if (respuesta.equals("OK")) {
                showPopupEliminar();
                //mostrarMensaje(FacesMessage.SEVERITY_INFO, properties.getMessage("SYS.OPERACION_EXITOSA"));
            } else if (respuesta.equals("ERROR")) {
                //mostrarMensaje(null, "Ha ocurrido un error.");
            } else {
                //mostrarMensaje(null, respuesta);
            }
        }
        
        
        
        

        return null;
    }

    public void setPopUpCrear(RichPopup popUpCrear) {
        this.popUpCrear = popUpCrear;
    }

    public RichPopup getPopUpCrear() {
        return popUpCrear;
    }

    public String commit_Modificar() {
        OperationBinding operationBinding = getBindings().getOperationBinding("Commit");
        operationBinding.execute();
        ocultaPopupModificar();
        popupModificar.cancel();
        return null;
    }
    
    public String commit_Crear() {
        OperationBinding operationBinding = getBindings().getOperationBinding("Commit");
        operationBinding.execute();
        ocultaPopupCrear();
        popUpCrear.cancel();        
        return null;
    }
    
    public String commit_Eliminar() {

        OperationBinding operationBinding = getBindings().getOperationBinding("Delete");
        operationBinding.execute();
        OperationBinding operationBinding2 = getBindings().getOperationBinding("Commit");
        operationBinding2.execute();

        ocultaPopupEliminar();
        popUpEliminar.cancel();
        return null;
    }

    public void setPopupModificar(RichPopup popupModificar) {
        this.popupModificar = popupModificar;
    }

    public RichPopup getPopupModificar() {
        return popupModificar;
    }

    public void rollback_Modificar(ActionEvent actionEvent) {
        OperationBinding operationBinding = getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
        ocultaPopupModificar();
        popupModificar.cancel();

    }
    
    public void rollback_Crear(ActionEvent actionEvent) {
        OperationBinding operationBinding = getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
        ocultaPopupCrear();
        popUpCrear.cancel();
    }

    public void cancel_Crear(PopupCanceledEvent popupCanceledEvent) {
        OperationBinding operationBinding = getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
        ocultaPopupCrear();
        popUpCrear.cancel();
    }
    
    public void cancel_Eliminar(PopupCanceledEvent popupCanceledEvent) {
        OperationBinding operationBinding = getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
        ocultaPopupEliminar();
        popUpEliminar.cancel();
    }

    public void setPopUpEliminar(RichPopup popUpEliminar) {
        this.popUpEliminar = popUpEliminar;
    }

    public RichPopup getPopUpEliminar() {
        return popUpEliminar;
    }
}
